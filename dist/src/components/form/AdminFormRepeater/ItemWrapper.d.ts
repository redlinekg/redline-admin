import { FunctionComponent, ReactElement } from "react";
export interface ItemWrapperProps {
    children: ReactElement | ReactElement[];
    onDelete?: () => void;
    parent_name: string;
    idx: number;
    label?: string;
    button_remove_text?: string;
}
/**
 * Component for wrap inputs and provide to inputs custom onChange prop
 * Children must to be instance of <Admin[input type]> and to hold name prop
 * When we change some item in inputs this component perform onChange prop(function)
 * @param {*} param
 */
export declare const ItemWrapper: FunctionComponent<ItemWrapperProps>;
export default ItemWrapper;
