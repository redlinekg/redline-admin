import { FunctionComponent } from "react";
export interface DeleteButtonProps {
    onClick?: ((e: any) => void) | undefined;
}
declare const DeleteButton: FunctionComponent<DeleteButtonProps>;
export default DeleteButton;
