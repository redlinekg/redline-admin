import React from "react";
import { AdminInputChangeValue, AdminInputProps, FormikAdminInputProps } from "./interfaces";
export interface AdminEditorJsProps extends AdminInputProps, AdminInputChangeValue {
    tools: {
        [key: string]: any;
    };
    exclude?: string[];
    image_upload_url: string;
    image_url_executor: string;
    image_form_data_field?: string;
    file_upload_url: string;
    file_form_data_field?: string;
    file_types: string[];
    link_endpoint: string;
}
export interface AdminEditorJsDefaultProps {
    image_form_data_field: string;
    file_form_data_field: string;
    exclude: string[];
}
declare const _default: React.ComponentType<AdminEditorJsProps & FormikAdminInputProps>;
export default _default;
