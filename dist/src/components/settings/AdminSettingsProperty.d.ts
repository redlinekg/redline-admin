import { PureComponent, ChangeEvent } from "react";
import { SettingsProperty } from "./interfaces";
export interface AdminSettingsPropertyProps {
    editable: boolean;
    onChangeProperty: (value: Partial<SettingsProperty>) => void;
    onDeleteProperty: (event?: any) => any;
    property: SettingsProperty;
}
declare class AdminSettingsProperty extends PureComponent<AdminSettingsPropertyProps> {
    changeTextValue: (event: ChangeEvent<HTMLInputElement>) => void;
    changeTextAreaValue: (event: ChangeEvent<HTMLTextAreaElement>) => void;
    changeSelectorValue: (value: string) => void;
    render(): JSX.Element;
}
export default AdminSettingsProperty;
