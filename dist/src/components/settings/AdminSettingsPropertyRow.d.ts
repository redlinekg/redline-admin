import React from "react";
import { AdminSettingsPropertyProps } from "./AdminSettingsProperty";
export interface AdminSettingsPropertyRowProps extends AdminSettingsPropertyProps {
    onAddNext: () => void;
    editable: boolean;
}
declare class AdminSettingsPropertyRow extends React.PureComponent<AdminSettingsPropertyRowProps> {
    render(): JSX.Element;
}
export default AdminSettingsPropertyRow;
