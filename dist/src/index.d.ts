import "core-js/stable";
import "regenerator-runtime/runtime";
import "antd/dist/antd.css";
import "./styles";
export * from "./components";
export * from "formik";
import * as antd from "antd";
export { antd };
