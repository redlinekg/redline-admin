/// <reference types="react" />
declare const adminSettingsContext: import("react").Context<{
    editable: boolean;
}>;
export default adminSettingsContext;
