import { Component } from "react";
import { SettingsSection, AddSection, DeleteSection, DeleteProperty, ChangeProperty, AddProperty, ChangeSection } from "./interfaces";
export interface AdminSettingsProps {
    settings?: SettingsSection[];
    editable: boolean;
    onChange: (sections: SettingsSection[]) => void;
}
export interface AdminSettingsDefaultProps {
    settings: SettingsSection[];
}
export interface AdminSettingsState {
    editable: boolean;
}
declare class AdminSettings extends Component<AdminSettingsProps, AdminSettingsState> {
    static defaultProps: AdminSettingsDefaultProps;
    static getDerivedStateFromProps(props: AdminSettingsProps): {
        editable: boolean;
    };
    state: AdminSettingsState;
    addSection: AddSection;
    deleteSection: DeleteSection;
    changeSection: ChangeSection;
    addProperty: AddProperty;
    changeProperty: ChangeProperty;
    deleteProperty: DeleteProperty;
    render(): JSX.Element;
}
export default AdminSettings;
