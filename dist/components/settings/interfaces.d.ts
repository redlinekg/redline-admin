export interface SettingsProperty {
    id: number | null;
    name: string;
    description: string;
    type: string | null;
    value: string;
}
export interface SettingsSection {
    id: number | null;
    title: string;
    description: string;
    properties: SettingsProperty[];
}
export declare type AddSection = () => void;
export declare type DeleteSection = (section_number: number) => void;
export declare type ChangeSection = (section_number: number, value: Partial<SettingsSection>) => void;
export declare type AddProperty = (section_number: number, property_number: number) => void;
export declare type ChangeProperty = (section_number: number, property_number: number, value: Partial<SettingsProperty>) => void;
export declare type DeleteProperty = (section_number: number, property_number: number) => void;
