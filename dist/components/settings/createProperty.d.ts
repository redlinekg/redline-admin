import { SettingsProperty, SettingsSection } from "./interfaces";
export declare function createProperty(): SettingsProperty;
export declare function createSection(): SettingsSection;
