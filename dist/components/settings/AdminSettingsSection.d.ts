/// <reference types="react" />
import { AddProperty, ChangeProperty, ChangeSection, DeleteProperty, DeleteSection, SettingsProperty } from "./interfaces";
export interface AdminSettingsSectionProps {
    editable: boolean;
    onAddNext: AddProperty;
    onDeleteSection: DeleteSection;
    onChangeProperty: ChangeProperty;
    onDeleteProperty: DeleteProperty;
    onChangeSection: ChangeSection;
    title: string;
    properties: SettingsProperty[];
    section_number: number;
}
declare const _default: (props: Omit<AdminSettingsSectionProps, "editable">) => JSX.Element;
export default _default;
