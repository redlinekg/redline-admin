import { PureComponent } from "react";
export interface AdminSettingsPropertyAddProps {
    onClick: () => void;
}
declare class AdminSettingsPropertyAdd extends PureComponent<AdminSettingsPropertyAddProps> {
    render(): JSX.Element;
}
export default AdminSettingsPropertyAdd;
