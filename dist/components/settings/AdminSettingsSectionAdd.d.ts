import PT from "prop-types";
import { PureComponent } from "react";
export interface AdminSettingsSectionAddProps {
    onAddSection: () => void;
    text?: string;
}
export interface AdminSettingsSectionAddDefaultProps {
    text: string;
}
declare class AdminSettingsSectionAdd extends PureComponent<AdminSettingsSectionAddProps> {
    static defaultProps: {
        text: string;
    };
    static propTypes: {
        onAddSection: PT.Requireable<(...args: any[]) => any>;
        text: PT.Requireable<string>;
    };
    render(): JSX.Element;
}
export default AdminSettingsSectionAdd;
