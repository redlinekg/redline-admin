import { Component } from "react";
import PT from "prop-types";
interface AdminFormBlockProps {
    children: any;
}
export default class AdminFormBlock extends Component<AdminFormBlockProps> {
    static propTypes: {
        children: PT.Requireable<any>;
    };
    render(): JSX.Element;
}
export {};
