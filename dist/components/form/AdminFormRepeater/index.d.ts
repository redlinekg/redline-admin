import React, { ReactElement } from "react";
import { FormikContextType } from "formik";
export interface AdminFormRepeaterProps {
    name: string;
    label: string;
    label_item: string;
    button_add_text: string;
    button_remove_text: string;
    children: ReactElement;
    formik: FormikContextType<{}>;
}
declare const _default: React.ComponentType<AdminFormRepeaterProps>;
export default _default;
