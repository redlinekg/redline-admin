import { FunctionComponent } from "react";
import { ButtonProps } from "antd/es/button";
export declare const AddItemButton: FunctionComponent<ButtonProps>;
export default AddItemButton;
