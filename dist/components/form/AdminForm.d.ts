import { FormikProps } from "formik";
import { IAnyModelType, IStateTreeNode } from "mobx-state-tree";
import PT from "prop-types";
import React, { Component } from "react";
interface AdminFormContextType {
    handleDelete?: (v: any) => void;
}
export declare const AdminFormContext: React.Context<AdminFormContextType>;
interface AdminFormProps {
    children: ((props: FormikProps<any>) => React.ReactNode) | React.ReactNode;
    item: IStateTreeNode;
    store: IAnyModelType;
    remove_show: boolean | null;
    onSubmitSuccess?: (type: "CHANGE" | "CREATE", result?: any) => any;
    onSubmitError?: (error?: any) => any;
    onDeleteSuccess?: (result?: any) => any;
    onDeleteError?: (error?: any) => any;
}
declare class AdminForm extends Component<AdminFormProps> {
    static defaultProps: {
        remove_show: boolean;
    };
    static propTypes: {
        children: PT.Requireable<any>;
        item: PT.Requireable<object>;
        store: PT.Requireable<object>;
        remove_show: PT.Requireable<boolean>;
        onSubmitSuccess: PT.Requireable<(...args: any[]) => any>;
        onDeleteSuccess: PT.Requireable<(...args: any[]) => any>;
        onSubmitError: PT.Requireable<(...args: any[]) => any>;
        onDeleteError: PT.Requireable<(...args: any[]) => any>;
    };
    state: {
        hasError: boolean;
    };
    handleSubmit: (values: {
        [key: string]: any;
    }) => Promise<void>;
    handleDelete: () => Promise<void>;
    componentDidCatch(error: Error): void;
    render(): JSX.Element;
}
export default AdminForm;
