import { FunctionComponent } from "react";
import { SaveButtonProps } from "../elements/AdminSaveButton";
declare const AdminSaveFormButton: FunctionComponent<SaveButtonProps>;
export default AdminSaveFormButton;
