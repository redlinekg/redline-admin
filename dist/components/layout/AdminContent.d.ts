import { Component } from "react";
import PT from "prop-types";
interface AdminContentProps {
    title: string;
    description: string;
    children: any;
    back_button_show: boolean;
    search: boolean;
    fit: boolean;
    login: boolean;
    routes: any;
    create_route: string;
    header: any;
    show_search: boolean;
    show_content_wrapper: boolean;
    show_bg: boolean;
    show_tools_line: boolean;
}
export default class AdminContent extends Component<AdminContentProps> {
    static propTypes: {
        title: PT.Validator<string>;
        description: PT.Requireable<string>;
        children: PT.Validator<any>;
        search: PT.Requireable<boolean>;
        fit: PT.Requireable<boolean>;
        login: PT.Requireable<boolean>;
        routes: PT.Requireable<object>;
        create_route: PT.Requireable<string>;
        back_button_show: PT.Requireable<boolean>;
        header: PT.Requireable<PT.ReactElementLike>;
        show_search: PT.Requireable<boolean>;
        show_content_wrapper: PT.Requireable<boolean>;
        show_bg: PT.Requireable<boolean>;
        show_tools_line: PT.Requireable<boolean>;
    };
    render(): JSX.Element;
}
export {};
