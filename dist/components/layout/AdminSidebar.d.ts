import { Component } from "react";
import PT from "prop-types";
interface AdminSidebarProps {
    menu_config: Array<any>;
    routes: any;
}
declare class AdminSidebar extends Component<AdminSidebarProps> {
    constructor(props: any);
    static propTypes: {
        menu_config: PT.Validator<any[]>;
        routes: PT.Validator<object>;
    };
    handleUpdate(values: any): void;
    toggleMenu(): void;
    renderThumb({ style, ...props }: {
        [x: string]: any;
        style: any;
    }): JSX.Element;
    render(): JSX.Element;
}
export default AdminSidebar;
