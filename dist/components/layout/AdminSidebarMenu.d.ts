import { PureComponent } from "react";
import PT from "prop-types";
import { RouteItem } from "../../interfaces";
interface AdminSidebarMenuProps {
    title?: string;
    menu: RouteItem;
    height: number | string;
}
export default class AdminSidebarMenu extends PureComponent<AdminSidebarMenuProps, {
    height: number | string;
}> {
    state: {
        height: string | number;
    };
    static propTypes: {
        title: PT.Requireable<string>;
        menu: PT.Requireable<object>;
        height: PT.Validator<any>;
    };
    toggle: () => void;
    closeMenu(): void;
    render(): JSX.Element;
}
export {};
