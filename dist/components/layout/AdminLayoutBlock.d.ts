import { Component } from "react";
import PT from "prop-types";
interface AdminLayoutBlockProps {
    children: any;
}
export default class AdminLayoutBlock extends Component<AdminLayoutBlockProps> {
    static propTypes: {
        children: PT.Validator<any>;
    };
    render(): JSX.Element;
}
export {};
