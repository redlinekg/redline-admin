import { Component } from "react";
import PT from "prop-types";
interface AdminHeaderProps {
    header_link: string;
    header_link_text: string;
    header_username: string;
    onLogout: () => void;
}
export default class AdminHeader extends Component<AdminHeaderProps> {
    static defaultProps: {
        header_link_text: string;
    };
    static propTypes: {
        header_link: PT.Requireable<string>;
        header_link_text: PT.Requireable<string>;
        header_username: PT.Requireable<string>;
        onLogout: PT.Requireable<(...args: any[]) => any>;
    };
    render(): JSX.Element;
}
export {};
