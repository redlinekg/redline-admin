import { PureComponent } from "react";
interface IconProps {
    name: string;
}
declare class Icon extends PureComponent<IconProps> {
    render(): JSX.Element;
}
export default Icon;
