import React from "react";
import PT from "prop-types";
declare class AdminHeaderButtonsPortal extends React.Component {
    static propTypes: {
        children: PT.Requireable<any>;
    };
    render(): JSX.Element;
}
export default AdminHeaderButtonsPortal;
