import { FunctionComponent } from "react";
import { RouteParams } from "../../interfaces";
export interface AdminCreateButtonProps {
    route: string;
    params?: RouteParams;
}
declare const AdminCreateButton: FunctionComponent<AdminCreateButtonProps>;
export default AdminCreateButton;
