import PT from "prop-types";
import { ReactElement } from "react";
import { RouteParams } from "../../interfaces";
export interface ActiveLinkProps {
    children: ReactElement;
    route: string;
    params?: RouteParams;
}
declare const ActiveLink: {
    ({ children, route, params, ...props }: ActiveLinkProps): JSX.Element;
    propTypes: {
        routes: PT.Requireable<object>;
        route: PT.Requireable<string>;
        params: PT.Requireable<object>;
        children: PT.Requireable<any>;
    };
};
export default ActiveLink;
