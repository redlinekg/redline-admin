import { Component } from "react";
import PT from "prop-types";
import { ButtonProps } from "antd/es/button";
interface AdminBackButtonProps extends ButtonProps {
    router: any;
}
declare class AdminBackButton extends Component<AdminBackButtonProps> {
    static propTypes: {
        router: PT.Requireable<object>;
    };
    back: () => void;
    render(): JSX.Element;
}
export default AdminBackButton;
