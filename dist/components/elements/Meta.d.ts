import { Component } from "react";
import PT from "prop-types";
export interface MetaProps {
    title: string;
    part: string;
}
declare class Meta extends Component<MetaProps> {
    static defaultProps: {
        part: string;
    };
    static propTypes: {
        title: PT.Validator<string>;
        part: PT.Requireable<string>;
    };
    render(): JSX.Element;
}
export default Meta;
