import { PureComponent } from "react";
import PT from "prop-types";
export default class Container extends PureComponent {
    static propTypes: {
        children: PT.Validator<any>;
    };
    render(): JSX.Element;
}
