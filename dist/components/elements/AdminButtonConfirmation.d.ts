import { ReactElement } from "react";
import PT from "prop-types";
import { ButtonProps } from "antd/es/button";
import { PopconfirmProps } from "antd/es/popconfirm";
export interface AdminButtonConfirmationProps {
    onClick: () => {};
    button_type: ButtonProps["type"];
    children: ReactElement;
    popconfirm_placement: PopconfirmProps["placement"];
}
declare const AdminButtonConfirmation: {
    ({ onClick, button_type, children, popconfirm_placement, }: AdminButtonConfirmationProps): JSX.Element;
    defaultProps: {
        popconfirm_placement: string;
    };
    propTypes: {
        onClick: PT.Requireable<(...args: any[]) => any>;
        button_type: PT.Requireable<string>;
        children: PT.Requireable<any>;
        popconfirm_placement: PT.Requireable<string>;
    };
};
export default AdminButtonConfirmation;
