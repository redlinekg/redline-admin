import { ButtonProps } from "antd/es/button";
import React from "react";
export interface SaveButtonProps extends ButtonProps {
    editable: boolean;
}
export declare const AdminSaveButton: React.ComponentType<SaveButtonProps>;
export default AdminSaveButton;
