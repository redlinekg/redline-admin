import { PureComponent } from "react";
import PT from "prop-types";
interface AdminTableOperationProps {
    item: any;
    remove?: boolean;
    movable?: boolean;
    route: string;
    params: {
        [key: string]: string;
    };
}
export default class AdminTableOperation extends PureComponent<AdminTableOperationProps> {
    static defaultProps: {
        remove: boolean;
        movable: boolean;
        params: {};
    };
    static propTypes: {
        item: PT.Validator<object>;
        remove: PT.Requireable<boolean>;
        movable: PT.Requireable<boolean>;
        route: PT.Validator<string>;
        params: PT.Requireable<object>;
    };
    render(): JSX.Element;
}
export {};
