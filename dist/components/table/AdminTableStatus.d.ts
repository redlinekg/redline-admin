import { PureComponent } from "react";
import PT from "prop-types";
interface AdminTableStatusProps {
    status: boolean;
    true_return: boolean | null;
    false_return: boolean | null;
}
export default class AdminTableStatus extends PureComponent<AdminTableStatusProps> {
    static propTypes: {
        status: PT.Validator<boolean>;
        true_return: PT.Requireable<string>;
        false_return: PT.Requireable<string>;
    };
    render(): JSX.Element;
}
export {};
