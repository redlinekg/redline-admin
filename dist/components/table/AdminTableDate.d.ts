import { PureComponent } from "react";
import PT from "prop-types";
interface AdminTableDateProps {
    date: string;
    time_show: boolean;
}
export default class AdminTableDate extends PureComponent<AdminTableDateProps> {
    static propTypes: {
        date: PT.Validator<string>;
        time_show: PT.Requireable<boolean>;
    };
    render(): JSX.Element;
}
export {};
