import { PureComponent } from "react";
import PT from "prop-types";
import { RouteParams } from "../../interfaces";
interface AdminTableTitleProps {
    title: string;
    route: string;
    params: RouteParams;
}
export default class AdminTableTitle extends PureComponent<AdminTableTitleProps> {
    static propTypes: {
        title: PT.Validator<string>;
        route: PT.Validator<string>;
        params: PT.Validator<object>;
    };
    render(): JSX.Element;
}
export {};
