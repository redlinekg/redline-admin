import { PureComponent } from "react";
import PT from "prop-types";
interface AdminTableTextProps {
    text: boolean | null;
    color_grey: boolean | null;
}
export default class AdminTableText extends PureComponent<AdminTableTextProps> {
    static propTypes: {
        text: PT.Requireable<string>;
        color_grey: PT.Requireable<boolean>;
    };
    render(): JSX.Element;
}
export {};
