import { PureComponent } from "react";
import PT from "prop-types";
interface AdminTableImageProps {
    src: string;
    size?: number;
}
export default class AdminTableImage extends PureComponent<AdminTableImageProps> {
    static propTypes: {
        src: PT.Validator<string>;
        size: PT.Requireable<number>;
    };
    render(): JSX.Element;
}
export {};
