import React from "react";
import { AdminInputProps, FormikAdminInputProps, SelectVariant, ValueConverter, ValuePreparer, VariantValuePreparer } from "./interfaces";
export interface AdminTreeSelectProps extends AdminInputProps {
    variants: SelectVariant[];
    validate?: () => {};
    onChange: (...args: any[]) => {};
    prepareVariantValue?: VariantValuePreparer;
    prepareValue?: ValuePreparer;
    convertValue?: ValueConverter;
}
export interface AdminTreeSelectDefaultProps {
    prepareVariantValue: VariantValuePreparer;
    prepareValue: ValuePreparer;
    convertValue: ValueConverter;
}
declare const _default: React.ComponentType<AdminTreeSelectProps & FormikAdminInputProps>;
export default _default;
