import React, { ReactElement } from "react";
import { Language } from "../../interfaces";
import { AdminInputProps, FormikAdminInputProps } from "./interfaces";
export interface AdminTranslateTabProps extends AdminInputProps {
    delimiter?: string;
    languages: Language[];
    default_language?: string;
    name: string;
    children: ReactElement;
    errors: {};
}
export interface AdminTranslateTabDefaultProps {
    default_language: string;
    delimiter: string;
}
declare const _default: React.ComponentType<AdminTranslateTabProps & FormikAdminInputProps>;
export default _default;
