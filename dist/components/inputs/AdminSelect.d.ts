import React from "react";
import { AdminInputChangeEvent, AdminInputProps, FormikAdminInputProps, SelectVariant, ValueConverter, ValuePreparer, VariantValuePreparer } from "./interfaces";
export interface AdminSelectProps extends AdminInputProps, AdminInputChangeEvent {
    variants: SelectVariant[];
    validate?: () => {};
    onChange: (...args: any[]) => {};
    prepareVariantValue?: VariantValuePreparer;
    prepareValue?: ValuePreparer;
    convertValue?: ValueConverter;
}
export interface AdminSelectDefaultProps {
    prepareVariantValue: VariantValuePreparer;
    prepareValue: ValuePreparer;
    convertValue: ValueConverter;
}
declare const _default: React.ComponentType<AdminSelectProps & FormikAdminInputProps>;
export default _default;
