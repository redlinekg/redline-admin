import React from "react";
import { AdminInputProps, FormikAdminInputProps, AdminInputChangeEvent } from "./interfaces";
export interface AdminNumberInputProps extends AdminInputProps, AdminInputChangeEvent {
}
declare const _default: React.ComponentType<AdminNumberInputProps & FormikAdminInputProps>;
export default _default;
