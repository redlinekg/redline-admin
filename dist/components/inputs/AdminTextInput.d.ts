import React from "react";
import { AdminInputProps, FormikAdminInputProps, AdminInputChangeEvent } from "./interfaces";
export interface AdminTextInputProps extends AdminInputProps, AdminInputChangeEvent {
}
declare const _default: React.ComponentType<AdminTextInputProps & FormikAdminInputProps>;
export default _default;
