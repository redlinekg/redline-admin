import { FormikContextType } from "formik";
export interface AdminInputProps {
    name: string;
    label: string;
    help: string;
    extra: string;
    translate_lang: string | undefined;
}
export interface AdminInputChangeValue {
    onChange: (v: any) => void;
}
export interface AdminInputChangeEvent {
    onChange: (v: any) => void;
}
export interface FormikAdminInputProps {
    formik: FormikContextType<any>;
}
export declare type VariantValuePreparer = (v: any) => any;
export declare type ValuePreparer = (v: any) => any;
export declare type ValueConverter = (v: any) => any;
export interface SelectVariant {
    value: any;
    label: string;
    children: SelectVariant[] | undefined;
}
export declare type UrlExecutor = (response_body: any) => string;
export declare type UploadType = "image" | "file";
export declare type MimeType = string;
export declare type MimeTypes = MimeType[];
export declare type UploadValuesConverter = (v: any) => {
    uid: string;
    name: string;
    status: string;
    url: string;
};
