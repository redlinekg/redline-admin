import React from "react";
import { AdminInputProps, FormikAdminInputProps } from "./interfaces";
export interface AdminTitleInputProps extends AdminInputProps {
    slug_name: string;
    slug_label: string;
    autoslug: boolean;
}
declare const _default: React.ComponentType<AdminTitleInputProps & FormikAdminInputProps>;
export default _default;
