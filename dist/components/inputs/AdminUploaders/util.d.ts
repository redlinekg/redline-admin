import { MimeTypes } from "../interfaces";
/**
 * create validator
 * @param {Array} allowed_types array of allowed file-types
 * @param {Number} size_limit max size of file
 */
export declare const createFileValidator: (allowed_types: MimeTypes, size_limit: number) => (file: File) => boolean;
export declare function getBase64(file: File): Promise<string>;
export declare const MIME_TYPES: string[];
