import { UploadFile } from "antd/es/upload/interface";
import React from "react";
import { AdminInputProps, FormikAdminInputProps, AdminInputChangeValue, UrlExecutor, MimeTypes, UploadType, UploadValuesConverter } from "../interfaces";
export interface AdminMultiUploaderProps extends AdminInputProps, AdminInputChangeValue {
    upload_url: string;
    url_executor?: UrlExecutor;
    form_data_field?: string;
    allowed_types?: MimeTypes;
    size_limit?: number;
    type?: UploadType;
    convertValue: UploadValuesConverter;
}
export interface AdminMultiUploaderDefaultProps {
    url_executor: UrlExecutor;
    form_data_field: string;
    allowed_types: MimeTypes;
    size_limit: number;
    type: UploadType;
}
export interface AdminMultiUploaderState {
    previewVisible: boolean;
    previewImage: string;
    localFileList: UploadFile[];
}
declare const _default: React.ComponentType<AdminMultiUploaderProps & FormikAdminInputProps>;
export default _default;
