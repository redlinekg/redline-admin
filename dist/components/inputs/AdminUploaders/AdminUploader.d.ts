import React from "react";
import { AdminInputChangeValue, AdminInputProps, FormikAdminInputProps, MimeTypes, UploadType, UrlExecutor } from "../interfaces";
export interface AdminUploaderProps extends AdminInputProps, AdminInputChangeValue {
    upload_url: string;
    url_executor?: UrlExecutor;
    form_data_field?: string;
    allowed_types?: MimeTypes;
    size_limit?: number;
    type?: UploadType;
}
export interface AdminUploaderDefaultProps {
    url_executor: UrlExecutor;
    form_data_field: string;
    allowed_types: MimeTypes;
    size_limit: number;
    type: UploadType;
}
export interface AdminUploaderState {
    loading: boolean;
    imageUrl: string | null;
}
declare const _default: React.ComponentType<AdminUploaderProps & FormikAdminInputProps>;
export default _default;
