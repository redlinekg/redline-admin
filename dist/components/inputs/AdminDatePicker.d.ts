import React from "react";
import { AdminInputProps, FormikAdminInputProps, AdminInputChangeEvent } from "./interfaces";
export interface AdminDatePickerProps extends AdminInputProps, AdminInputChangeEvent {
}
declare const _default: React.ComponentType<AdminDatePickerProps & FormikAdminInputProps>;
export default _default;
