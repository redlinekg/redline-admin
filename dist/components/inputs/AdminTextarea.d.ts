import React from "react";
import { AdminInputChangeEvent, AdminInputProps, FormikAdminInputProps } from "./interfaces";
export interface AdminTextareaProps extends AdminInputProps, AdminInputChangeEvent {
}
declare const _default: React.ComponentType<AdminTextareaProps & FormikAdminInputProps>;
export default _default;
