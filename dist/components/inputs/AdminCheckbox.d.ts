import React from "react";
import { AdminInputProps, FormikAdminInputProps, AdminInputChangeEvent } from "./interfaces";
export interface AdminCheckboxProps extends AdminInputProps, AdminInputChangeEvent {
}
declare const _default: React.ComponentType<AdminCheckboxProps & FormikAdminInputProps>;
export default _default;
