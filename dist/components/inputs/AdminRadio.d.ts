import React from "react";
import { AdminInputProps, FormikAdminInputProps, AdminInputChangeEvent, SelectVariant } from "./interfaces";
export interface AdminRadioProps extends AdminInputProps, AdminInputChangeEvent {
    variants: SelectVariant[];
}
declare const _default: React.ComponentType<AdminRadioProps & FormikAdminInputProps>;
export default _default;
