/// <reference types="react" />
interface AdminFormContextType {
    handleDelete?: (v: any) => void;
}
export declare const AdminFormContext: import("react").Context<AdminFormContextType>;
export {};
