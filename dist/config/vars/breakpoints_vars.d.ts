export declare type BREAKPOINT = "xs" | "es" | "sm" | "md" | "lg" | "xl";
export declare type BREAKPOINT_DOWN = Exclude<BREAKPOINT, "xl">;
export declare const grid: {
    [key in BREAKPOINT]: number;
};
export declare const grid_down: {
    [key in BREAKPOINT]: number;
};
export declare const container: {
    [key in Exclude<BREAKPOINT, "es">]: number;
};
export declare const up: (min: BREAKPOINT) => string;
export declare const down: (max: BREAKPOINT) => string;
export declare const only: (min: BREAKPOINT, max: BREAKPOINT) => string;
