export declare const global_color: {
    grey: string;
};
export declare const colors: {
    white: string;
    black: string;
    blue: string;
    red: string;
    violet: string;
    yellow: string;
    green: string;
    grey: string;
    grey_bor: string;
    grey_light: string;
    grey_light2: string;
    grey_light3: string;
    grey_light4: string;
    grey_light5: string;
    grey_light6: string;
};
