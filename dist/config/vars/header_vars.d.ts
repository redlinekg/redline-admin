export declare const header_num: {
    height: number;
    height_top: number;
    height_bottom: number;
};
export declare const header: {
    height: string;
    height_top: string;
    height_bottom: string;
};
