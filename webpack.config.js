/* eslint-disable */
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;

//
const isProd = process.env.NODE_ENV === "production";
const ANALYZE = !!process.env.ANALYZE;

module.exports = {
    mode: isProd ? "production" : "development",
    // devtool: "source-map",
    entry: ["./src/index.ts"],
    target: "webworker",
    output: {
        path: path.resolve(__dirname, "dist"),
        libraryTarget: "umd",
        library: "redline-admin",
        umdNamedDefine: true,
        globalObject: "this",
    },
    resolve: {
        alias: {
            // antd: path.resolve(__dirname, "node_modules/antd/dist/antd.min.js"),
            axios: path.resolve(
                __dirname,
                "node_modules/axios/dist/axios.min.js"
            ),
            moment: path.resolve(
                __dirname,
                "node_modules/moment/min/moment-with-locales.min.js"
            ),
        },
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx|js|jsx)$/,
                resolve: {
                    extensions: [".ts", ".tsx", ".js", ".jsx"],
                },
                exclude: /node_modules/,
                use: [
                    { loader: "babel-loader" },
                    {
                        loader: "linaria/loader",
                        options: {},
                    },
                    {
                        loader: "ts-loader",
                        options: {
                            transpileOnly: false,
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: isProd,
                        },
                    },
                    {
                        loader: "css-loader",
                        options: { sourceMap: isProd },
                    },
                    {
                        loader: "postcss-loader",
                    },
                ],
            },
            {
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8192,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "redline-admin.css",
            chunkFilename: "[id].css",
            ignoreOrder: false,
        }),
        ...(ANALYZE ? [new BundleAnalyzerPlugin()] : []),
    ],
    externals: {
        react: { commonjs2: "react", commonjs: "react", amd: "react" },
        "react-dom": {
            commonjs2: "react-dom",
            commonjs: "react-dom",
            amd: "react-dom",
        },
        "react-helmet": {
            commonjs2: "react-helmet",
            commonjs: "react-helmet",
            amd: "react-helmet",
        },
        next: { commonjs2: "next", commonjs: "next", amd: "next" },
        "next/head": {
            commonjs2: "next/head",
            commonjs: "next/head",
            amd: "next/head",
        },
        "next/router": {
            commonjs2: "next/router",
            commonjs: "next/router",
            amd: "next/router",
        },
        mobx: {
            commonjs2: "mobx",
            commonjs: "mobx",
            amd: "mobx",
        },
        "mobx-react": {
            commonjs2: "mobx-react",
            commonjs: "mobx-react",
            amd: "mobx-react",
        },
        "mobx-state-tree": {
            commonjs2: "mobx-state-tree",
            commonjs: "mobx-state-tree",
            amd: "mobx-state-tree",
        },
        linaria: {
            commonjs2: "linaria",
            commonjs: "linaria",
            amd: "linaria",
        },
    },
};
