export const header_num = {
    height: 120,
    height_top: 60,
    height_bottom: 50,
};

export const header = {
    height: `${header_num.height_top + header_num.height_bottom}px`,
    height_top: `${header_num.height_top}px`,
    height_bottom: `${header_num.height_bottom}px`,
};
