import { lighten } from "polished";

export const global_color = {
    grey: "#5A6372",
};

export const colors = {
    // global
    white: "#ffffff",
    black: "#151515",
    blue: "#006CA1",
    red: "#ed253f",
    violet: "#5d78ff",
    yellow: "#FCC32E",
    green: "#26A69B",

    // grey colors
    grey: global_color.grey,
    grey_bor: lighten(0.57, global_color.grey),
    grey_light: lighten(0.3, global_color.grey),
    grey_light2: lighten(0.4, global_color.grey),
    grey_light3: lighten(0.5, global_color.grey),
    grey_light4: lighten(0.55, global_color.grey),
    grey_light5: lighten(0.58, global_color.grey),
    grey_light6: lighten(0.6, global_color.grey),
};