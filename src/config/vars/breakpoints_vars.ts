export type BREAKPOINT = "xs" | "es" | "sm" | "md" | "lg" | "xl";
export type BREAKPOINT_DOWN = Exclude<BREAKPOINT, "xl">;

const BREAKPOINTS: BREAKPOINT[] = ["xs", "es", "sm", "md", "lg", "xl"];

export const grid: { [key in BREAKPOINT]: number } = {
    xs: 0,
    es: 362,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200,
};

export const grid_down: { [key in BREAKPOINT]: number } = {
    xs: grid.es,
    es: grid.es - 0.02,
    sm: grid.sm - 0.02,
    md: grid.md - 0.02,
    lg: grid.lg - 0.02,
    xl: grid.xl - 0.02,
};

export const container: { [key in Exclude<BREAKPOINT, "es">]: number } = {
    xs: 352,
    sm: 540,
    md: 720,
    lg: 960,
    xl: 1140,
};

export const up = (min: BREAKPOINT) => `@media (min-width: ${grid[min]}px)`;
export const down = (max: BREAKPOINT) =>
    `@media (max-width: ${grid_down[max]}px)`;
export const only = (min: BREAKPOINT, max: BREAKPOINT) => {
    const min_idx = BREAKPOINTS.indexOf(min);
    const max_idx = BREAKPOINTS.indexOf(max);
    if (max_idx <= min_idx) {
        throw Error("max cannot to be less/equal then min");
    }
    return `@media and (min-width: ${grid[min]}px) and (max-width: ${grid_down[max]}px)`;
};
