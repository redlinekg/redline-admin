import React from "react";
import { styled } from "linaria/react";
import Table, { TableProps } from "antd/es/table";

import { down } from "../../config/vars";

const AdminTable = (props: TableProps<any>) => (
    <STable>
        <SBlock>
            <Table rowKey="id" {...props} />
        </SBlock>
    </STable>
);

const STable = styled.div`
    ${down("md")} {
        max-width: 100%;
        overflow-x: auto;
    }
`;
const SBlock = styled.div`
    ${down("md")} {
        /* width: 700px; */
        width: unset;
    }
`;

export default AdminTable;
