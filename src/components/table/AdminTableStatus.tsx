import React, { PureComponent } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";

interface AdminTableStatusProps {
    status: boolean;
    true_return: boolean | null;
    false_return: boolean | null;
}

export default class AdminTableStatus extends PureComponent<
    AdminTableStatusProps
> {
    static propTypes = {
        status: PT.bool.isRequired,
        true_return: PT.string,
        false_return: PT.string,
    };

    render() {
        const { status, true_return, false_return } = this.props;
        return (
            <>
                {status === true ? (
                    <SStatus className="enabled">
                        <span>{true_return ? true_return : "Вкл"}</span>
                    </SStatus>
                ) : (
                    <SStatus className="disabled">
                        <span>{false_return ? false_return : "Выкл"}</span>
                    </SStatus>
                )}
            </>
        );
    }
}

const SStatus = styled.div`
    color: var(--white);
    padding: 0.1em 0.6em;
    border-radius: 20px;
    white-space: nowrap;
    display: inline-block;
    &.enabled {
        background-color: var(--green);
    }
    &.disabled {
        background-color: var(--red);
    }
`;
