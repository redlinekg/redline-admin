import React, { PureComponent } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";

import { Link } from "..";
import { RouteParams } from "../../interfaces";

interface AdminTableTitleProps {
    title: string;
    route: string;
    params: RouteParams;
}

export default class AdminTableTitle extends PureComponent<
    AdminTableTitleProps
> {
    static propTypes = {
        title: PT.string.isRequired,
        route: PT.string.isRequired,
        params: PT.object.isRequired,
    };
    render() {
        const { title, route, params } = this.props;
        return (
            <SAdminTableTitle>
                <Link route={route} params={params}>
                    <a>{title}</a>
                </Link>
            </SAdminTableTitle>
        );
    }
}

const SAdminTableTitle = styled.div`
    display: inline-block;
    a {
        font-size: 1.1em;
    }
`;
