import React, { PureComponent } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";

interface AdminTableTextProps {
    text: boolean | null;
    color_grey: boolean | null;
}

export default class AdminTableText extends PureComponent<AdminTableTextProps> {
    static propTypes = {
        text: PT.string,
        color_grey: PT.bool,
    };

    render() {
        const { text, color_grey } = this.props;
        return (
            <>
                {text ? (
                    <SAdminTableText
                        className={color_grey ? "color_grey" : undefined}
                    >
                        {text}
                    </SAdminTableText>
                ) : null}
            </>
        );
    }
}

const SAdminTableText = styled.div`
    white-space: nowrap;
    &.color_grey {
        color: var(--grey-light);
    }
`;
