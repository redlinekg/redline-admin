import React, { PureComponent } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import Avatar from "antd/es/avatar";

interface AdminTableImageProps {
    src: string;
    size?: number;
}

export default class AdminTableImage extends PureComponent<
    AdminTableImageProps
> {
    static propTypes = {
        src: PT.string.isRequired,
        size: PT.number,
    };
    render() {
        const { src, size } = this.props;
        return (
            <SAdminTableImage>
                <Avatar src={src} size={size ? size : 64} shape="square" />
            </SAdminTableImage>
        );
    }
}

const SAdminTableImage = styled.div`
    width: 6em;
    img {
        /* object-fit: cover; */
        object-fit: contain;
        object-position: center center;
    }
`;
