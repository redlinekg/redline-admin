import React, { PureComponent } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import moment from "moment";

interface AdminTableDateProps {
    date: string;
    time_show: boolean;
}

export default class AdminTableDate extends PureComponent<AdminTableDateProps> {
    static propTypes = {
        date: PT.string.isRequired,
        time_show: PT.bool,
    };
    render() {
        const m = moment(this.props.date);
        return (
            <SAdminTableDate>
                {this.props.time_show
                    ? m.locale("ru").format("DD MMMM YYYY HH:mm")
                    : m.locale("ru").format("DD MMMM YYYY")}
            </SAdminTableDate>
        );
    }
}

const SAdminTableDate = styled.div``;
