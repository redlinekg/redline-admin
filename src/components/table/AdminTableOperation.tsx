import React, { PureComponent } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import Icon from "antd/es/icon";
import Button from "antd/es/button";
import Popconfirm from "antd/es/popconfirm";

import { Link } from "..";

interface AdminTableOperationProps {
    item: any;
    remove?: boolean;
    movable?: boolean;
    route: string;
    params: { [key: string]: string };
}

export default class AdminTableOperation extends PureComponent<
    AdminTableOperationProps
> {
    static defaultProps = {
        remove: true,
        movable: false,
        params: {},
    };

    static propTypes = {
        item: PT.object.isRequired,
        remove: PT.bool,
        movable: PT.bool,
        route: PT.string.isRequired,
        params: PT.object,
    };

    render() {
        const { remove, route, params, item, movable } = this.props;
        return (
            <SAdminTableOperation>
                {route ? (
                    <Link route={route} params={params}>
                        <a>
                            <Icon type="form" />
                        </a>
                    </Link>
                ) : null}
                {remove && item && item.delete instanceof Function ? (
                    <Popconfirm
                        title="Вы действительно хотите удалить?"
                        okText="Да"
                        cancelText="Нет"
                        placement="topRight"
                        onConfirm={item.delete}
                    >
                        <a href="#">
                            <Icon type="delete" />
                        </a>
                    </Popconfirm>
                ) : null}
                {movable ? (
                    <>
                        <Button onClick={item.up} type="default">
                            Up
                        </Button>
                        <Button onClick={item.down} type="default">
                            Dw
                        </Button>
                    </>
                ) : null}
            </SAdminTableOperation>
        );
    }
}

const SAdminTableOperation = styled.div`
    display: flex;
    a {
        display: block;
        width: 1.6em;
        font-size: 1.2em;
    }
`;
