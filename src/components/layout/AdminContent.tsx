import React, { Component } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import CN from "classnames";
import { position } from "polished";

import { AdminBackButton, AdminCreateButton, Meta } from "..";
import RoutesContext from "../../contexts/RoutesContext";
import { down } from "../../config/vars";

interface AdminContentProps {
    title: string;
    description: string;
    children: any;
    back_button_show: boolean;
    search: boolean;
    fit: boolean;
    login: boolean;
    routes: any;
    create_route: string;
    header: any;
    show_search: boolean;
    show_content_wrapper: boolean;
    show_bg: boolean;
    show_tools_line: boolean;
}

export default class AdminContent extends Component<AdminContentProps> {
    static propTypes = {
        title: PT.string.isRequired,
        description: PT.string,
        children: PT.any.isRequired,
        search: PT.bool,
        fit: PT.bool,
        login: PT.bool,

        routes: PT.object, // next-routes
        // route to create page
        create_route: PT.string,
        back_button_show: PT.bool,
        //
        header: PT.element,
        show_search: PT.bool,
        show_content_wrapper: PT.bool,
        show_bg: PT.bool,
        show_tools_line: PT.bool,
    };
    render() {
        const {
            title,
            description,
            children,
            routes,
            create_route,
            back_button_show,
            // visual
            show_content_wrapper,
            show_bg,
            // new
            header,
            // tools
            show_search,
            show_tools_line,
        } = this.props;
        return (
            <RoutesContext.Provider
                value={{
                    routes,
                }}
            >
                <SHeaderFix>
                    <Meta
                        title={`${title} ${description ? description : ""}`}
                    />
                    {header ? header : null}
                    {show_tools_line ? (
                        <SNavigation>
                            <SNavigationTitle>
                                <SH1>{title}</SH1>
                                {description && (
                                    <SNavigationSub>
                                        {description}
                                    </SNavigationSub>
                                )}
                            </SNavigationTitle>
                            {show_search ? <></> : null}
                            <SNavigationAction>
                                {back_button_show ? (
                                    <AdminBackButton router={routes.Router} />
                                ) : null}
                                {create_route && (
                                    <AdminCreateButton route={create_route} />
                                )}
                                <SButtonsBlock id="header_buttons"></SButtonsBlock>
                            </SNavigationAction>
                        </SNavigation>
                    ) : null}
                </SHeaderFix>
                <SAdminContent>
                    <SBlock
                        className={CN({
                            fit: show_content_wrapper,
                            hide_bg: show_bg,
                        })}
                    >
                        {children}
                    </SBlock>
                </SAdminContent>
            </RoutesContext.Provider>
        );
    }
}

const SHeaderFix = styled.div`
    ${position("fixed", "0", null, null, "var(--global-sidebar-width)")};
    width: calc(100% - var(--global-sidebar-width));
    height: var(--header-height);
    z-index: 10;
    border-bottom: 1px solid var(--grey-bor);
    ${down("md")} {
        position: relative;
        left: 0;
        width: 100%;
        z-index: auto;
    }
`;
const SAdminContent = styled.div`
    background-color: var(--grey-light4);
    padding-top: calc(var(--header-height) + var(--global-padding-content));
    padding-right: var(--global-padding-content);
    padding-bottom: var(--global-padding-content);
    padding-left: var(--global-padding-content);
    width: 100%;
    min-height: 100vh;
    ${down("md")} {
        padding-top: 1em;
        padding-bottom: 10em;
    }
`;
const SBlock = styled.div`
    background-color: var(--white);
    width: 100%;
    border-radius: var(--global-border-radius);
    padding: 2em;
    &.fit {
        padding: 0;
    }
    &.hide_bg {
        background-color: transparent;
    }
    ${down("md")} {
        background-color: transparent;
        padding: 0;
    }
    .ant-table-thead,
    .ant-table-tbody {
        & > tr {
            & > th {
                padding: 16px 10px;
                &:first-of-type {
                    padding-left: 25px;
                }
                &:last-of-type {
                    padding-left: 25px;
                }
            }
            & > td {
                padding: 16px 10px;
                &:first-of-type {
                    padding-left: 25px;
                }
                &:last-of-type {
                    padding-left: 25px;
                }
            }
        }
    }

    .ant-pagination {
        padding-right: 25px;
    }
`;
const SNavigation = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 var(--global-padding-content);
    height: var(--header-height-bottom);
    background-color: var(--white);
    box-shadow: 0 10px 30px 0 rgba(82, 63, 105, 0.08);
    ${down("md")} {
        flex-wrap: wrap;
        padding: 1.2em var(--global-padding-content) 1em;
    }
`;
const SNavigationTitle = styled.div`
    display: flex;
    align-items: center;
    ${down("md")} {
        margin-bottom: 1em;
    }
`;
const SH1 = styled.h1`
    font-weight: 700;
    font-size: 1.2em;
    margin: 0;
`;
const SSearch = styled.div``;
//////////////////
//////////////////
//////////////////
const SNavigationSub = styled.div`
    padding-left: 0.5em;
`;
const SNavigationAction = styled.div`
    font-size: 0.9em;
    ${down("md")} {
        display: flex;
        justify-content: space-between;
        width: 100%;
    }
    button {
        margin-left: 0.5em;
        ${down("md")} {
            margin-left: 1em;
            &:first-child {
                margin-left: 0;
            }
        }
    }
`;

const SButtonsBlock = styled.div`
    display: inline-block;
`;
