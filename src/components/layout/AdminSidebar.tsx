import React, { Component } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import { position } from "polished";
import { Scrollbars } from "react-custom-scrollbars";

import { AdminSidebarMenu } from "..";
import { down } from "../../config/vars";
import RoutesContext from "../../contexts/RoutesContext";
import logo_admin from "../../static/img/logo_admin.svg";

interface AdminSidebarProps {
    menu_config: Array<any>;
    routes: any;
}

type MyState = { top: number };

class AdminSidebar extends Component<AdminSidebarProps, MyState> {
    constructor(props: any) {
        super(props);
        this.state = { top: 0 };
        this.toggleMenu = this.toggleMenu.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.renderThumb = this.renderThumb.bind(this);
    }

    static propTypes = {
        menu_config: PT.array.isRequired,
        routes: PT.object.isRequired,
    };

    handleUpdate(values) {
        const { top } = values;
        this.setState({ top });
    }

    toggleMenu() {
        const _body = document.getElementsByTagName("body")[0];
        if (_body.classList.contains("mobile_menu_active")) {
            _body.classList.remove("mobile_menu_active");
        } else {
            _body.classList.add("mobile_menu_active");
        }
    }

    renderThumb({ style, ...props }) {
        return <SScrollThumb style={{ ...style }} {...props} />;
    }

    render() {
        const { routes, menu_config } = this.props;
        return (
            <RoutesContext.Provider value={{ routes }}>
                <SAdminSidebar className="SAdminSidebar">
                    <SLogoBlock>
                        <SLogo
                            className="logo"
                            src={logo_admin}
                            alt="Admin logo"
                        />
                        <BtnMobileMenu onClick={this.toggleMenu}>
                            <span />
                        </BtnMobileMenu>
                    </SLogoBlock>
                    <SScrollBlock>
                        <Scrollbars renderThumbVertical={this.renderThumb}>
                            {menu_config && menu_config.length && (
                                <>
                                    {menu_config.map((l, idx) => (
                                        <AdminSidebarMenu
                                            key={idx}
                                            menu={l}
                                            height={l.height}
                                        />
                                    ))}
                                </>
                            )}
                        </Scrollbars>
                    </SScrollBlock>
                </SAdminSidebar>
            </RoutesContext.Provider>
        );
    }
}

const SAdminSidebar = styled.div`
    ${position("fixed", "0", null, null, "0")};
    width: var(--global-sidebar-width);
    height: 100vh;
    background-color: var(--sidebar_bg);
    z-index: 11;
    ${down("md")} {
        position: relative;
        height: auto;
        width: 100%;
        .mobile_menu_active & {
            position: fixed;
        }
    }
`;

////////////////
////////////////
////////////////

const SLogoBlock = styled.div`
    display: flex;
    align-items: center;
    height: var(--header-height-top);
    padding: 0 var(--global-padding-content);
    background-color: var(--black);
    ${down("md")} {
    }
`;
const SLogo = styled.img`
    width: 7em;
    ${down("md")} {
        width: 5em;
    }
`;
const SScrollBlock = styled.div`
    height: calc(100vh - var(--header-height-top));
    ${down("md")} {
        display: none;
        .mobile_menu_active & {
            display: block;
        }
    }
`;

const SScrollThumb = styled.div`
    background-color: var(--white);
    opacity: 0.8;
    transition: opacity all 0.1s;
    &:hover {
        opacity: 1;
    }
`;

const BtnMobileMenu = styled.button`
    margin-left: 1.5em;
    background: none;
    border: 0;
    padding: 0;
    position: relative;
    z-index: 22;
    cursor: pointer;
    transition: all 250ms cubic-bezier(0.4, 0, 0.2, 1);
    display: none;
    ${down("md")} {
        display: block;
    }
    span {
        width: 24px;
        height: 3px;
        margin: 10px 0;
        background: var(--white);
        position: relative;
        border-radius: 2px;
        transition: all 250ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
        display: block;
        .mobile_menu_active & {
            background: 0 0;
        }
        &:before,
        &:after {
            content: "";
            border-radius: 2px;
            height: 3px;
            background: var(--white);
            position: absolute;
            transition: all 250ms cubic-bezier(0.4, 0, 0.2, 1);
            left: 0;
            .mobile_menu_active & {
                background: var(--red);
                top: 0;
                width: 24px;
            }
        }
        &:before {
            top: -7px;
            width: 20px;
            .mobile_menu_active & {
                transform: rotate(45deg);
            }
        }
        &:after {
            top: 7px;
            width: 16px;
            .mobile_menu_active & {
                transform: rotate(-45deg);
            }
        }
    }
`;

export default AdminSidebar;
