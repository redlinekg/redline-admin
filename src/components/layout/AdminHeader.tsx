import React, { Component } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import Avatar from "antd/es/avatar";
import Button from "antd/es/button";
import Dropdown from "antd/es/dropdown";
import Menu from "antd/es/menu";

import { down } from "../../config/vars";

interface AdminHeaderProps {
    header_link: string;
    header_link_text: string;
    header_username: string;
    onLogout: () => void;
}

export default class AdminHeader extends Component<AdminHeaderProps> {
    static defaultProps = {
        header_link_text: "Перейти",
    };

    static propTypes = {
        header_link: PT.string,
        header_link_text: PT.string,
        header_username: PT.string,
        onLogout: PT.func,
    };
    render() {
        const {
            header_link,
            header_link_text,
            header_username,
            onLogout,
        } = this.props;
        return (
            <SAdminHeader className="SAdminHeader">
                <SContainer>
                    <SBlock>
                        <SHeaderLink>
                            {header_link && (
                                <a href={header_link} target="blank">
                                    <Button>{header_link_text}</Button>
                                </a>
                            )}
                        </SHeaderLink>
                        {header_username ? (
                            <Dropdown
                                overlay={
                                    <Menu>
                                        <Menu.Item>
                                            <a onClick={onLogout}>Выйти</a>
                                        </Menu.Item>
                                    </Menu>
                                }
                                placement="bottomLeft"
                            >
                                <SUser>
                                    <SUserName>{header_username}</SUserName>
                                    <Avatar shape="square" icon="user" />
                                </SUser>
                            </Dropdown>
                        ) : null}
                    </SBlock>
                </SContainer>
            </SAdminHeader>
        );
    }
}

const SAdminHeader = styled.div`
    position: relative;
    max-width: 100%;
    background: var(--white);
    border-bottom: 1px solid var(--grey-bor);
    ${down("md")} {
        position: absolute;
        right: 15px;
        top: -40px;
        z-index: 11;
        border: 0;
        background: none;
    }
`;

const SContainer = styled.div`
    padding: 0 var(--global-padding-content);
    ${down("md")} {
        padding: 0;
    }
`;

const SHeaderLink = styled.div`
    ${down("md")} {
        display: none;
    }
`;

const SBlock = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: var(--header-height-top);
    ${down("md")} {
        height: auto;
    }
`;
const SUser = styled.div`
    display: flex;
    align-items: center;
`;

const SUserName = styled.div`
    color: var(--grey);
    margin-right: 0.8em;
    ${down("sm")} {
        display: none;
    }
`;
