import React, { PureComponent } from "react";
import PT from "prop-types";
import CN from "classnames";
import { styled } from "linaria/react";
import { lighten } from "polished";
import AnimateHeight from "react-animate-height";

import { Link } from "../index";
import { RouteItem } from "../../interfaces";

interface IconProps {
    name: string;
}

class Icon extends PureComponent<IconProps> {
    static propTypes = {
        name: PT.string.isRequired,
    };

    render() {
        const { name } = this.props;
        return (
            <svg>
                <use xlinkHref={`#${name}`} />
            </svg>
        );
    }
}

interface MenuListLinkProps {
    item: RouteItem;
}

class MenuListLink extends PureComponent<MenuListLinkProps> {
    static defaultProps = {
        item: {
            route: "",
            slug: "",
            title: "",
        },
    };

    static propTypes = {
        item: PT.object,
    };

    render() {
        const {
            item: { route, slug, title },
        } = this.props;
        return (
            <SMenuListLink>
                <Link route={route} params={slug ? { slug } : undefined}>
                    <a>
                        <i />
                        <span>{title}</span>
                    </a>
                </Link>
            </SMenuListLink>
        );
    }
}

const SMenuListLink = styled.li`
    display: block;
    font-size: 0.9em;
    a {
        position: relative;
        display: block;
        padding: 0.6em 40px;
        font-weight: 500;
        color: var(--grey-light);
        display: flex;
        align-items: center;
        transition: all 0.2s;
        i {
            width: 1.2em;
            &:before {
                content: "";
                display: block;
                width: 0.3em;
                height: 0.3em;
                background-color: var(--grey-light);
                border-radius: 50%;
            }
        }
        &.active,
        &:hover {
            background-color: var(--black);
            color: var(--white);
            i {
                &:before {
                    background-color: var(--red);
                }
            }
        }
    }
`;

interface AdminSidebarMenuProps {
    title?: string;
    menu: RouteItem;
    height: number | string;
}

export default class AdminSidebarMenu extends PureComponent<
    AdminSidebarMenuProps,
    { height: number | string }
> {
    state = {
        height: this.props.height,
    };
    static propTypes = {
        title: PT.string,
        menu: PT.object,
        height: PT.any.isRequired,
    };
    toggle = () => {
        const { height } = this.state;
        this.setState({
            height: height === 0 ? "auto" : 0,
        });
    };
    closeMenu() {
        const _body = document.getElementsByTagName("body")[0];
        _body.classList.remove("mobile_menu_active");
    }
    render() {
        const {
            menu: { route, slug, title, sub, icon, delimiter, group_title },
        } = this.props;
        const { height } = this.state;
        return (
            <SAdminSidebarMenu>
                {group_title ? <SGroupTitle>{group_title}</SGroupTitle> : null}
                {route ? (
                    <SMenuLink onClick={this.closeMenu}>
                        <Link
                            route={route}
                            params={slug ? { slug } : undefined}
                        >
                            <a>
                                {icon && <Icon name={icon} />}
                                <span>{title}</span>
                                {sub && sub.length && <SMenuArrow />}
                            </a>
                        </Link>
                    </SMenuLink>
                ) : (
                    <SMenuButton
                        onClick={this.toggle}
                        className={CN({
                            active: height === 0 ? false : true,
                        })}
                    >
                        {icon && <Icon name={icon} />}
                        <span>{title}</span>
                        {sub && sub.length && <SMenuArrow />}
                    </SMenuButton>
                )}

                {sub && sub.length && (
                    <AnimateHeight
                        duration={200}
                        height={height}
                        animateOpacity
                    >
                        <SMenuList className="SMenuList">
                            {sub.map((l: any, idx: number) => (
                                <MenuListLink key={idx} item={l} />
                            ))}
                        </SMenuList>
                    </AnimateHeight>
                )}
                {delimiter ? <SDelimiter /> : null}
            </SAdminSidebarMenu>
        );
    }
}

const SAdminSidebarMenu = styled.div`
    margin: 1em 0 0.8em;
    text-align: right;
    &:last-of-type {
        margin-bottom: 3em;
    }
`;

const SMenuLinkIcon = {
    width: "1.5em",
    height: "1.5em",
    marginRight: "0.8em",
};

const SMenuLinkStyle = {
    display: "flex",
    alignItems: "center",
    position: "relative",
    padding: "0.6em 20px",
    color: "var(--grey-light)",
    fontWeight: "600",
    cursor: "pointer",
    transition: "all 0.2s",
};

const SMenuLink = styled.div`
    a {
        transition: transform 0.25s ease;
        ${SAdminSidebarMenu}:hover & {
            transform: translateX(5px);
        }
        ${SMenuLinkStyle};
        &:hover {
            color: var(--white);
        }
        svg {
            ${SMenuLinkIcon};
        }
        &.active {
            background-color: var(--black);
            color: var(--red);
            transform: translateX(0) !important;
            cursor: default;
        }
    }
`;
const SMenuButton = styled.div`
    ${SMenuLinkStyle};
    &:hover {
        color: var(--white);
    }
    svg {
        ${SMenuLinkIcon};
    }
    &.active {
        color: var(--white);
    }
`;
const SMenuArrow = styled.div`
    margin-left: auto;
    transition: transform 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    position: relative;
    width: 0.3em;
    height: 0.3em;
    &:before,
    &:after {
        position: absolute;
        width: 6px;
        height: 1.5px;
        background: var(--grey-light);
        border-radius: 2px;
        transition: background 0.3s cubic-bezier(0.645, 0.045, 0.355, 1),
            transform 0.3s cubic-bezier(0.645, 0.045, 0.355, 1),
            top 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
        content: "";
    }
    &:before {
        transform: rotate(-45deg) translateX(2px);
    }
    &:after {
        transform: rotate(45deg) translateX(-2px);
    }
    ${SMenuButton}.active & {
        transform: translateY(-2px);
        &:before {
            transform: rotate(45deg) translateX(2px);
            background: var(--white);
        }
        &:after {
            transform: rotate(-45deg) translateX(-2px);
            background: var(--white);
        }
    }
`;
const SMenuList = styled.ul`
    padding-top: 0.5em;
    padding-left: 0;
`;
const SDelimiter = styled.div`
    width: 100%;
    border-bottom: 1px solid #232323;
    margin: 2em 0;
`;
const SGroupTitle = styled.div`
    padding: 0 0 1em 20px;
    text-align: left;
    color: var(--grey);
`;
