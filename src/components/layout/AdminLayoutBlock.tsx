import React, { Component } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";

import { down } from "../../config/vars";

interface AdminLayoutBlockProps {
    children: any;
}

export default class AdminLayoutBlock extends Component<AdminLayoutBlockProps> {
    static propTypes = {
        children: PT.any.isRequired,
    };
    render() {
        const { children } = this.props;
        return <SAdminLayoutBlock>{children}</SAdminLayoutBlock>;
    }
}

const SAdminLayoutBlock = styled.div`
    position: relative;
    padding-left: var(--global-sidebar-width);
    ${down("md")} {
        padding-left: 0;
    }
`;
