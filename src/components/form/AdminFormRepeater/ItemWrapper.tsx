import React, {
    Children,
    cloneElement,
    FunctionComponent,
    ReactElement,
    ReactNode,
} from "react";
import Button from "antd/es/button";
import { styled } from "linaria/react";
import Icon from "antd/es/icon";
import Popconfirm from "antd/es/popconfirm";

type RepeaterItemRenderComponent = (item_info: {
    parent_name: string;
    idx: number;
}) => ReactNode;

export interface ItemWrapperProps {
    children: ReactElement | ReactElement[] | RepeaterItemRenderComponent;
    onDelete?: () => void;
    parent_name: string;
    idx: number;
    label?: string;
    button_remove_text?: string;
}

/**
 * Component for wrap inputs and provide to inputs custom onChange prop
 * Children must to be instance of <Admin[input type]> and to hold name prop
 * When we change some item in inputs this component perform onChange prop(function)
 * @param {*} param
 */
export const ItemWrapper: FunctionComponent<ItemWrapperProps> = ({
    children,
    onDelete,
    parent_name,
    idx,
    label,
    button_remove_text,
}: ItemWrapperProps) => (
    <SItemWrapper className="SItemWrapper">
        <STitle className="STitle">
            <SNum>#{idx + 1}</SNum>
            {label && <SLabel>{label}</SLabel>}
        </STitle>
        {typeof children === "function"
            ? children({ parent_name, idx })
            : Children.map(Children.toArray(children), (child) => {
                  if (!child.props || !child.props.name) {
                      throw new Error(
                          "You must to define name for inputs inside <AdminFormRepeater/>"
                      );
                  }
                  const name = child.props.name;
                  return cloneElement(child, {
                      name: `${parent_name}[${idx}].${name}`,
                  });
              })}
        <SRemove>
            {onDelete ? (
                <Popconfirm
                    title="Вы действительно хотите удалить?"
                    okText="Да"
                    cancelText="Нет"
                    placement="topLeft"
                    onConfirm={onDelete}
                >
                    <Button type="dashed">
                        <Icon
                            className="dynamic-delete-button"
                            type="minus-circle-o"
                        />{" "}
                        {button_remove_text} #{idx + 1}
                    </Button>
                </Popconfirm>
            ) : null}
        </SRemove>
    </SItemWrapper>
);

// ItemWrapper.propTypes = {
//     children: PT.any,
//     onDelete: PT.func,
//     idx: PT.number,
//     parent_name: PT.string,
//     label: PT.string,
//     button_remove_text: PT.string
// };

const SItemWrapper = styled.div`
    margin-bottom: 3em;
    &:nth-last-of-type(1),
    &:nth-last-of-type(2) {
        margin-bottom: 2em;
    }
    .SItemWrapper {
        padding: 2em 2em 2em 2em;
        background: var(--grey-light5);
        border: 1px solid var(--grey-light3);
        margin-bottom: 1em;
        border-radius: var(--global-border-radius);
        .ant-form-item {
            &:last-of-type {
                margin-bottom: 0;
            }
        }
        .STitle {
        }
        .SItemWrapper {
            background: #fff9f0;
            border: 1px solid #f3eadd;
        }
    }
    .SAdd {
        text-align: center;
        text-align: right;
        position: relative;
        top: 0;
        margin-bottom: 2em;
        button {
            width: 100%;
        }
    }
`;
const STitle = styled.div`
    display: flex;
    align-items: center;
    border-bottom: 1px solid var(--grey-light3);
    padding-bottom: 0.5em;
    margin-bottom: 0.5em;
    font-weight: 800;
    font-size: 1.2em;
`;
const SLabel = styled.div``;
const SNum = styled.div`
    padding-right: 0.4em;
    color: var(--red);
`;
const SRemove = styled.div``;

export default ItemWrapper;
