import React, { ReactElement } from "react";
import { styled } from "linaria/react";
import { connect, FormikContextType } from "formik";
import get from "lodash/get";

import { AddItemButton } from "./AddItemButton";
import { ItemWrapper } from "./ItemWrapper";

export interface AdminFormRepeaterProps {
    name: string;
    label: string;
    label_item: string;
    button_add_text: string;
    button_remove_text: string;
    children: ReactElement;
    formik: FormikContextType<{}>;
}

class AdminFormRepeater extends React.Component<AdminFormRepeaterProps> {
    static defaultProps = {
        button_add_text: "Добавить",
        button_remove_text: "Удалить",
    };

    /**
     * Add new item to list
     */
    addItem = () => {
        const {
            formik: { setFieldValue },
            name,
        } = this.props;
        const current_value = this.getValue();
        if (current_value) {
            setFieldValue(name, [...current_value, {}]);
        } else {
            setFieldValue(name, [{}]);
        }
    };
    /**
     *  Remove item from list
     */
    removeItem = (idx: number) => {
        const {
            formik: { setFieldValue },
            name,
        } = this.props;
        const current_value = this.getValue();
        if (current_value) {
            setFieldValue(
                name,
                (current_value as any[]).filter((_, _idx) => _idx !== idx)
            );
        }
    };

    getValue = () => {
        const {
            formik: { values },
            name,
        } = this.props;
        const value = get(values, name);
        return value;
    };

    getErrors = (): string[] | string | Object[] | undefined => {
        const {
            formik: { errors },
            name,
        } = this.props;
        return get(errors, name);
    };

    render() {
        const {
            children,
            name,
            button_add_text,
            button_remove_text,
            label,
            label_item,
        } = this.props;
        let items: any[] | ReactElement[] | ReactElement = [];
        const formik_value = this.getValue();
        const formik_errors = this.getErrors();

        if (Array.isArray(formik_value)) {
            items = formik_value.map((v, idx) => (
                <ItemWrapper
                    key={idx}
                    idx={idx}
                    parent_name={name}
                    label={label_item}
                    button_remove_text={button_remove_text}
                    onDelete={() => this.removeItem(idx)}
                >
                    {children}
                </ItemWrapper>
            ));
        }

        return (
            <SAdminFormRepeater>
                <SBlock>
                    {label && <STitle className="STitleLabel">{label}</STitle>}
                    {items}
                    {formik_errors && formik_errors.length ? (
                        <SErrorsList>
                            {Array.isArray(formik_errors) ? (
                                formik_errors
                                    .filter(
                                        (err: any) => typeof err === "string"
                                    )
                                    .map((err: any, idx: number) => (
                                        <SErrorsItem key={idx}>
                                            {err}
                                        </SErrorsItem>
                                    ))
                            ) : (
                                <SErrorsItem>{formik_errors}</SErrorsItem>
                            )}
                        </SErrorsList>
                    ) : null}
                    <SAdd className="SAdd">
                        <AddItemButton onClick={this.addItem} type="primary">
                            {button_add_text}
                        </AddItemButton>
                    </SAdd>
                </SBlock>
            </SAdminFormRepeater>
        );
    }
}

const SAdminFormRepeater = styled.div`
    .ant-popover-message-title {
        padding-left: 2em;
    }
`;
const SBlock = styled.div``;
const STitle = styled.h2`
    font-size: 1.4em;
    font-weight: 800;
    margin-bottom: 1em;
    color: var(--black);
`;

const SErrorsList = styled.ul``;

const SErrorsItem = styled.li`
    color: var(--red);
`;

const SAdd = styled.div``;

export default connect(AdminFormRepeater);
