import React, { FunctionComponent } from "react";
import PT from "prop-types";
import Icon from "antd/es/icon";
import Button, { ButtonProps } from "antd/es/button";

export const AddItemButton: FunctionComponent<ButtonProps> = ({
    children,
    ...props
}: ButtonProps) => (
    <Button type="dashed" {...props}>
        <Icon type="plus" /> {children}
    </Button>
);

AddItemButton.propTypes = {
    children: PT.any,
};

export default AddItemButton;
