import Form from "antd/es/form";
import React, { FunctionComponent } from "react";
import { AdminSaveButton, SaveButtonProps } from "../elements/AdminSaveButton";

const AdminSaveFormButton: FunctionComponent<SaveButtonProps> = (
    props: SaveButtonProps
) => (
    <Form.Item>
        <AdminSaveButton {...props} />
    </Form.Item>
);

export default AdminSaveFormButton;
