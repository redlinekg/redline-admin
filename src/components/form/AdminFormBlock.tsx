import React, { Component } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";

import { down } from "../../config/vars";

interface AdminFormBlockProps {
    children: any;
}

export default class AdminFormBlock extends Component<AdminFormBlockProps> {
    static propTypes = {
        children: PT.any,
    };
    render() {
        const { children } = this.props;
        return <SAdminFormBlock>{children}</SAdminFormBlock>;
    }
}

const SAdminFormBlock = styled.div`
    max-width: 700px;
    ${down("md")} {
        max-width: none;
    }
    .ant-form-item-label {
        text-align: left;
    }
    form textarea.ant-input {
    }
`;
