import message from "antd/es/message";
import { Formik, FormikProps } from "formik";
import { Form } from "formik-antd";
import { observer } from "mobx-react";
import { getSnapshot, IAnyModelType, IStateTreeNode } from "mobx-state-tree";
import PT from "prop-types";
import React, { Component, createContext } from "react";
import {
    AdminDeleteButton,
    AdminHeaderButtonsPortal,
    AdminSaveButton,
} from "../";
import AdminFormBlock from "./AdminFormBlock";

const DEFAULT_FORM_ITEM_LAYOUT = {
    labelCol: {
        xs: { span: 10 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 14 },
        sm: { span: 14 },
    },
};

interface AdminFormContextType {
    handleDelete?: (v: any) => void;
}

export const AdminFormContext = createContext<AdminFormContextType>({});

interface AdminFormProps {
    children: ((props: FormikProps<any>) => React.ReactNode) | React.ReactNode;
    item: IStateTreeNode;
    store: IAnyModelType;
    remove_show: boolean | null;
    onSubmitSuccess?: (type: "CHANGE" | "CREATE", result?: any) => any;
    onSubmitError?: (error?: any) => any;
    onDeleteSuccess?: (result?: any) => any;
    onDeleteError?: (error?: any) => any;
}

@observer
class AdminForm extends Component<AdminFormProps> {
    static defaultProps = {
        remove_show: true,
    };

    static propTypes = {
        children: PT.any,
        item: PT.object,
        store: PT.object,
        remove_show: PT.bool,
        // callbacks
        onSubmitSuccess: PT.func,
        onDeleteSuccess: PT.func,
        onSubmitError: PT.func,
        onDeleteError: PT.func,
    };

    state = {
        hasError: false,
    };

    handleSubmit = async (values: { [key: string]: any }) => {
        const { item, store, onSubmitSuccess, onSubmitError } = this.props;
        try {
            if (!item && store) {
                const result = await store.create(values);
                message.success("Успешно добавленно");
                if (onSubmitSuccess) {
                    onSubmitSuccess("CREATE", result);
                }
            } else if (item) {
                const result = await (item as any).update(values);
                message.success("Успешно изменено");
                if (onSubmitSuccess) {
                    onSubmitSuccess("CHANGE", result);
                }
            } else {
                throw new Error("Item and store is not defined in <AdminForm>");
            }
        } catch (error) {
            message.error("Произошла ошибка");
            if (onSubmitError) {
                onSubmitError(error);
            } else {
                throw error;
            }
        }
    };

    handleDelete = async () => {
        const { item, onDeleteSuccess, onDeleteError } = this.props;
        try {
            const result = await (item as any).delete();
            onDeleteSuccess(result);
        } catch (error) {
            onDeleteError(error);
        }
    };

    componentDidCatch(error: Error) {
        this.setState({
            hasError: true,
        });
    }

    render() {
        const { children, item, remove_show, ...rest } = this.props;
        const { hasError } = this.state;
        const item_data = item ? getSnapshot(item) : {};
        return hasError ? (
            <div>Error!(((</div>
        ) : (
            <AdminFormBlock>
                <AdminFormContext.Provider
                    value={{ handleDelete: this.handleDelete }}
                >
                    <Formik
                        initialValues={item_data}
                        onSubmit={this.handleSubmit}
                        enableReinitialize
                        {...rest}
                    >
                        {(...args) => (
                            <>
                                <Form {...DEFAULT_FORM_ITEM_LAYOUT}>
                                    {children instanceof Function
                                        ? children(...args)
                                        : children}
                                </Form>
                                <AdminHeaderButtonsPortal>
                                    <AdminSaveButton editable={!!item} />
                                    {item && remove_show ? (
                                        <AdminDeleteButton />
                                    ) : null}
                                </AdminHeaderButtonsPortal>
                            </>
                        )}
                    </Formik>
                </AdminFormContext.Provider>
            </AdminFormBlock>
        );
    }
}

export default AdminForm;
