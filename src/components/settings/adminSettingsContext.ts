import { createContext } from "react";
const adminSettingsContext = createContext<{
    editable: boolean;
}>({ editable: false });
export default adminSettingsContext;
