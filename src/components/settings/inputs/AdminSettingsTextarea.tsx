import Input from "antd/es/input";
import React, { Component } from "react";
const { TextArea } = Input;

class AdminSettingsTextarea extends Component {
    render() {
        return <TextArea name="text" autosize={{ minRows: 2, maxRows: 20 }} />;
    }
}

export default AdminSettingsTextarea;
