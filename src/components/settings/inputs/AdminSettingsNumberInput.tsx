import InputNumber from "antd/es/input-number";
import React, { Component } from "react";

class AdminSettingsNumberInput extends Component {
    render() {
        return <InputNumber name="text" min={1} max={10} defaultValue={3} />;
    }
}

export default AdminSettingsNumberInput;
