import Input from "antd/es/input";
import React, { Component } from "react";

class AdminSettingsTextInput extends Component {
    render() {
        return <Input name="text" />;
    }
}

export default AdminSettingsTextInput;
