import React, { ChangeEvent, PureComponent } from "react";
import { styled } from "linaria/react";
import Input from "antd/es/input";

import { AdminButtonConfirmation, AdminSettingsPropertyAdd } from "../index";
import adminSettingsContext from "./adminSettingsContext";
import AdminSettingsPropertyRow from "./AdminSettingsPropertyRow";
import {
    AddProperty,
    ChangeProperty,
    ChangeSection,
    DeleteProperty,
    DeleteSection,
    SettingsProperty,
} from "./interfaces";
import { down, up } from "../../config/vars";

export interface AdminSettingsSectionProps {
    editable: boolean;
    onAddNext: AddProperty;
    onDeleteSection: DeleteSection;
    onChangeProperty: ChangeProperty;
    onDeleteProperty: DeleteProperty;
    onChangeSection: ChangeSection;
    title: string;
    properties: SettingsProperty[];
    section_number: number;
}

/**
 * @prop {Object} properties - list of properties like this { value: string, type: string, property_name: string }
 */
class AdminSettingsSection extends PureComponent<AdminSettingsSectionProps> {
    // static propTypes = {
    //     editable: PT.bool,
    //     onAddNext: PT.func,
    //     onDeleteSection: PT.func,
    //     onChangeProperty: PT.func,
    //     onDeleteProperty: PT.func,
    //     onChangeSection: PT.func,
    //     title: PT.string,
    //     properties: PT.array,
    //     section_number: PT.number
    // };

    onChangeTitle = (event: ChangeEvent<HTMLInputElement>) => {
        const { onChangeSection, section_number } = this.props;
        if (onChangeSection) {
            onChangeSection(section_number, { title: event.target.value });
        }
    };

    render() {
        const {
            editable,
            onAddNext: _onAddNext,
            onDeleteSection: _onDeleteSection,
            onChangeProperty: _onChangeProperty,
            onDeleteProperty: _onDeleteProperty,
            section_number,
            title,
            properties,
        } = this.props;
        const onAddNext = (property_number: number) =>
            _onAddNext(section_number, property_number);
        const onDeleteSection = () => _onDeleteSection(section_number);
        const onDeleteProperty = (property_number: number) =>
            _onDeleteProperty(section_number, property_number);
        const onChangeProperty = (
            property_number: number,
            value: Partial<SettingsProperty>
        ) => _onChangeProperty(section_number, property_number, value);
        return (
            <SAdminSettingsSection>
                <SWrap>
                    <STitleInput>
                        <Input
                            defaultValue={title}
                            disabled={!editable}
                            onChange={this.onChangeTitle}
                        />
                    </STitleInput>
                    <SBlock>
                        {editable ? (
                            <AdminSettingsPropertyAdd
                                onClick={() => onAddNext(-1)}
                            />
                        ) : null}
                        {properties.map((property, idx) => (
                            <AdminSettingsPropertyRow
                                key={property.id || idx + 1000}
                                editable={editable}
                                onAddNext={() => onAddNext(idx)}
                                onDeleteProperty={() => onDeleteProperty(idx)}
                                onChangeProperty={(
                                    value: Partial<SettingsProperty>
                                ) => {
                                    onChangeProperty(idx, value);
                                }}
                                property={property}
                            />
                        ))}
                    </SBlock>
                    {editable ? (
                        <>
                            <br />
                            <AdminButtonConfirmation
                                button_type="dashed"
                                onClick={onDeleteSection as () => {}}
                            >
                                <span>Удалить группу</span>
                            </AdminButtonConfirmation>
                        </>
                    ) : null}
                </SWrap>
            </SAdminSettingsSection>
        );
    }
}

const SAdminSettingsSection = styled.div`
    margin-bottom: 2em;
    ${down("sm")} {
        margin-bottom: 3em;
    }
`;
const SWrap = styled.div`
    ${up("md")} {
        background-color: var(--white);
        border-radius: var(--global-border-radius);
        padding: 2em;
    }
`;
const SBlock = styled.div`
    width: 100%;
    max-width: 1000px;
`;
const STitleInput = styled.div`
    margin: 0 0 1.2em;
    input {
        font-weight: 800;
        font-size: 1.9em;
        padding: 0;
        border: 0;
        color: var(--black);
        &.ant-input-disabled {
            color: var(--black);
            background-color: transparent;
        }
    }
`;

export default (props: Omit<AdminSettingsSectionProps, "editable">) => (
    <adminSettingsContext.Consumer>
        {({ editable }) => (
            <AdminSettingsSection {...props} editable={editable} />
        )}
    </adminSettingsContext.Consumer>
);
