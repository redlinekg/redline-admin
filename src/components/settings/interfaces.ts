export interface SettingsProperty {
    id: number | null;
    name: string;
    description: string;
    type: string | null;
    value: string;
}

export interface SettingsSection {
    id: number | null;
    title: string;
    description: string;
    properties: SettingsProperty[];
}

export type AddSection = () => void;
export type DeleteSection = (section_number: number) => void;
export type ChangeSection = (
    section_number: number,
    value: Partial<SettingsSection>
) => void;
export type AddProperty = (
    section_number: number,
    property_number: number
) => void;

export type ChangeProperty = (
    section_number: number,
    property_number: number,
    value: Partial<SettingsProperty>
) => void;

export type DeleteProperty = (
    section_number: number,
    property_number: number
) => void;
