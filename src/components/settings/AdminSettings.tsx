import React, { Component } from "react";
import { styled } from "linaria/react";

import { AdminSettingsSection, AdminSettingsSectionAdd } from "../index";
import adminSettingsContext from "./adminSettingsContext";
import { createProperty, createSection } from "./createProperty";
import {
    SettingsSection,
    SettingsProperty,
    AddSection,
    DeleteSection,
    DeleteProperty,
    ChangeProperty,
    AddProperty,
    ChangeSection,
} from "./interfaces";

export interface AdminSettingsProps {
    settings?: SettingsSection[];
    editable: boolean;
    onChange: (sections: SettingsSection[]) => void;
}

export interface AdminSettingsDefaultProps {
    settings: SettingsSection[];
}

export interface AdminSettingsState {
    editable: boolean;
}

class AdminSettings extends Component<AdminSettingsProps, AdminSettingsState> {
    static defaultProps: AdminSettingsDefaultProps = {
        settings: [],
    };

    static getDerivedStateFromProps(props: AdminSettingsProps) {
        return {
            editable: props.editable,
        };
    }

    state: AdminSettingsState = {
        editable: false,
    };

    addSection: AddSection = () => {
        const { settings, onChange } = this.props as AdminSettingsProps &
            AdminSettingsDefaultProps;
        if (onChange) {
            onChange([...settings, createSection()]);
        }
    };

    deleteSection: DeleteSection = (section_number: number) => {
        const { settings, onChange } = this.props as AdminSettingsProps &
            AdminSettingsDefaultProps;
        if (onChange) {
            onChange(
                settings.filter((_, idx) => {
                    return idx !== section_number;
                })
            );
        }
    };

    changeSection: ChangeSection = (
        section_number: number,
        value: Partial<SettingsSection>
    ) => {
        const { settings, onChange } = this.props as AdminSettingsProps &
            AdminSettingsDefaultProps;
        const result = settings.map((section, idx) => {
            if (section_number === idx) {
                return {
                    ...section,
                    ...value,
                };
            }
            return section;
        });
        if (onChange) {
            onChange(result);
        }
    };

    addProperty: AddProperty = (
        section_number: number,
        property_number: number
    ) => {
        const { settings, onChange } = this.props as AdminSettingsProps &
            AdminSettingsDefaultProps;
        const new_property = createProperty();
        const result = settings.map((section, idx) => {
            if (section_number === idx) {
                return {
                    ...section,
                    properties: section.properties.length
                        ? section.properties.reduce(
                              (
                                  acc: SettingsProperty[],
                                  current: SettingsProperty,
                                  idx: number
                              ) => {
                                  // property_number may be '-1' then new_property will add to start of properties array
                                  if (property_number === -1 && idx === 0) {
                                      acc = [new_property];
                                  }
                                  acc = [...acc, current];
                                  if (idx === property_number) {
                                      return [...acc, new_property];
                                  }
                                  return acc;
                              },
                              []
                          )
                        : [new_property],
                };
            }
            return section;
        });
        if (onChange) {
            onChange(result);
        }
    };

    changeProperty: ChangeProperty = (
        section_number: number,
        property_number: number,
        value: Partial<SettingsProperty>
    ) => {
        const { settings, onChange } = this.props as AdminSettingsProps &
            AdminSettingsDefaultProps;
        const result = settings.map((section, idx) => {
            if (section_number === idx) {
                return {
                    ...section,
                    properties: section.properties.map((property, idx) => {
                        if (property_number === idx) {
                            return {
                                ...property,
                                ...value,
                            };
                        }
                        return property;
                    }),
                };
            }
            return section;
        });
        if (onChange) {
            onChange(result);
        }
    };
    deleteProperty: DeleteProperty = (
        section_number: number,
        property_number: number
    ) => {
        const { settings, onChange } = this.props as AdminSettingsProps &
            AdminSettingsDefaultProps;
        const result = settings.map((section, idx) => {
            if (section_number === idx) {
                return {
                    ...section,
                    properties: section.properties.filter((_, idx) => {
                        return property_number !== idx;
                    }),
                };
            }
            return section;
        });
        if (onChange) {
            onChange(result);
        }
    };

    render() {
        const { editable } = this.state;
        const { settings } = this.props as AdminSettingsProps &
            AdminSettingsDefaultProps;
        return (
            <SAdminSettings>
                <adminSettingsContext.Provider
                    value={{
                        editable,
                    }}
                >
                    <>
                        {settings.map((section, idx) => (
                            <AdminSettingsSection
                                key={section.id || idx + 1000}
                                onAddNext={this.addProperty}
                                onChangeProperty={this.changeProperty}
                                onDeleteSection={this.deleteSection}
                                onChangeSection={this.changeSection}
                                onDeleteProperty={this.deleteProperty}
                                section_number={idx}
                                {...section}
                            />
                        ))}
                        {editable ? (
                            <AdminSettingsSectionAdd
                                onAddSection={this.addSection}
                                text="Добавить еще группу"
                            />
                        ) : null}
                    </>
                </adminSettingsContext.Provider>
            </SAdminSettings>
        );
    }
}

const SAdminSettings = styled.div`
    position: relative;
`;

export default AdminSettings;
