import Button from "antd/es/button";
import Icon from "antd/es/icon";
import PT from "prop-types";
import React, { PureComponent } from "react";

export interface AdminSettingsSectionAddProps {
    onAddSection: () => void;
    text?: string;
}

export interface AdminSettingsSectionAddDefaultProps {
    text: string;
}

class AdminSettingsSectionAdd extends PureComponent<
    AdminSettingsSectionAddProps
> {
    static defaultProps = {
        text: "Добавить еще",
    };

    static propTypes = {
        onAddSection: PT.func,
        text: PT.string,
    };

    render() {
        const { text, onAddSection, ...another } = this
            .props as AdminSettingsSectionAddProps &
            AdminSettingsSectionAddDefaultProps;
        return (
            <Button type="dashed" onClick={onAddSection} {...another}>
                <Icon type="plus" /> {text}
            </Button>
        );
    }
}

export default AdminSettingsSectionAdd;
