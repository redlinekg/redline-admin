import { SettingsProperty, SettingsSection } from "./interfaces";
export function createProperty(): SettingsProperty {
    return {
        id: null,
        name: "",
        description: "",
        type: null,
        value: "",
    };
}

export function createSection(): SettingsSection {
    return {
        id: null,
        title: "",
        description: "",
        properties: [createProperty()],
    };
}
