import React from "react";
import AdminSettingsProperty, {
    AdminSettingsPropertyProps,
} from "./AdminSettingsProperty";
import AdminSettingsPropertyAdd from "./AdminSettingsPropertyAdd";

export interface AdminSettingsPropertyRowProps
    extends AdminSettingsPropertyProps {
    onAddNext: () => void;
    editable: boolean;
}

class AdminSettingsPropertyRow extends React.PureComponent<
    AdminSettingsPropertyRowProps
> {
    // static propTypes = {
    //     onAddNext: PT.func,
    //     editable: PT.bool
    // };
    render() {
        const { editable, onAddNext, ...rest } = this.props;
        return (
            <>
                <AdminSettingsProperty editable={editable} {...rest} />

                {editable ? (
                    <AdminSettingsPropertyAdd onClick={onAddNext} />
                ) : null}
            </>
        );
    }
}

export default AdminSettingsPropertyRow;
