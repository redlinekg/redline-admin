import React, { PureComponent, ChangeEvent } from "react";
import { styled } from "linaria/react";
import Input from "antd/es/input";
import Select from "antd/es/select";

const { TextArea } = Input;

import { AdminButtonConfirmation } from "../index";
import { SettingsProperty } from "./interfaces";

const { Option } = Select;

export interface AdminSettingsPropertyProps {
    editable: boolean;
    onChangeProperty: (value: Partial<SettingsProperty>) => void;
    onDeleteProperty: (event?: any) => any;
    property: SettingsProperty;
}

class AdminSettingsProperty extends PureComponent<AdminSettingsPropertyProps> {
    changeTextValue = (event: ChangeEvent<HTMLInputElement>) => {
        const { property, onChangeProperty } = this.props;
        if (onChangeProperty) {
            onChangeProperty({
                ...property,
                [event.target.name]: event.target.value,
            });
        }
    };

    changeTextAreaValue = (event: ChangeEvent<HTMLTextAreaElement>) => {
        const { property, onChangeProperty } = this.props;
        if (onChangeProperty) {
            onChangeProperty({
                ...property,
                [event.target.name]: event.target.value,
            });
        }
    };

    changeSelectorValue = (value: string) => {
        const { property, onChangeProperty } = this.props;
        if (onChangeProperty) {
            onChangeProperty({
                ...property,
                type: value,
            });
        }
    };

    render() {
        const {
            editable,
            property: { name, description, type, value },
            onDeleteProperty,
        } = this.props;
        return (
            <SAdminSettingsProperty
                className={editable ? "editable_true" : "editable_false"}
            >
                <SBlock>
                    {editable ? (
                        <SField>
                            <Input
                                value={name}
                                placeholder="Name field"
                                name="name"
                                onChange={this.changeTextValue}
                            />
                        </SField>
                    ) : null}
                    <SField className="field_name">
                        <Input
                            value={description}
                            placeholder="Name field"
                            name="description"
                            disabled={!editable}
                            onChange={this.changeTextValue}
                        />
                    </SField>
                    {editable ? (
                        <SField>
                            <Select
                                showSearch
                                placeholder="Тип поля"
                                value={type || undefined}
                                onChange={this.changeSelectorValue}
                            >
                                <Option value="text">Text</Option>
                                <Option value="textarea">TextArea</Option>
                            </Select>
                        </SField>
                    ) : null}
                    <SField>
                        {type === "text" ? (
                            <Input
                                placeholder="Значение"
                                name="value"
                                value={value}
                                onChange={this.changeTextValue}
                            />
                        ) : type === "textarea" ? (
                            <TextArea
                                placeholder="Значение"
                                name="value"
                                value={value}
                                autosize={{ minRows: 4, maxRows: 20 }}
                                onChange={this.changeTextAreaValue}
                            />
                        ) : null}
                    </SField>
                    {editable ? (
                        <SField>
                            <AdminButtonConfirmation
                                button_type="dashed"
                                onClick={onDeleteProperty}
                                popconfirm_placement="topRight"
                            >
                                <span>Удалить</span>
                            </AdminButtonConfirmation>
                        </SField>
                    ) : null}
                </SBlock>
            </SAdminSettingsProperty>
        );
    }
}

const SAdminSettingsProperty = styled.div`
    &.editable_false {
        margin-bottom: 1em;
        padding-bottom: 1em;
        border-bottom: 1px solid var(--grey-light4);
        &:last-of-type {
            margin-bottom: 0;
            padding-bottom: 0;
            border-bottom: 0;
        }
    }
`;
const SBlock = styled.div`
    display: flex;
`;
const SField = styled.div`
    flex: 1;
    transition: all 0.1s;
    display: flex;
    ${SAdminSettingsProperty}.editable_true & {
        padding: 0 0 0 2em;
    }
    &.field_name {
        ${SAdminSettingsProperty}.editable_false & {
            flex: 0 0 24%;
            padding-right: 2em;
            input {
                padding: 0;
                border: 0;
                &.ant-input-disabled {
                    color: var(--black);
                    background-color: transparent;
                }
            }
        }
    }
    .ant-select,
    .ant-input-number,
    .ant-input {
        width: 100%;
    }
`;

export default AdminSettingsProperty;
