import React, { PureComponent } from "react";
import { styled } from "linaria/react";
import Icon from "antd/es/icon";
import { position } from "polished";

export interface AdminSettingsPropertyAddProps {
    onClick: () => void;
}

class AdminSettingsPropertyAdd extends PureComponent<
    AdminSettingsPropertyAddProps
> {
    render() {
        const { onClick } = this.props;
        return (
            <SAdd className={"editable"}>
                <SIcon onClick={onClick}>
                    <Icon type="plus" />
                </SIcon>
                <SLine />
            </SAdd>
        );
    }
}

const SAdd = styled.div`
    width: 100%;
    position: relative;
    transition: all 0.2s;
    height: 1em;
    &.editable {
        height: 2em;
    }
`;
const SIcon = styled.div`
    ${position("absolute", "50%", null, null, "-0.5em")};
    transform: translateY(-50%);
    border: 1px solid var(--grey-bor);
    background-color: var(--white);
    z-index: 2;
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.14);
    display: flex;
    align-items: center;
    justify-content: center;
    width: 1.4em;
    height: 1.4em;
    border-radius: 50%;
    cursor: pointer;
    transition: all 0.2s;
    ${SAdd}:hover & {
        color: var(--green);
    }
    svg {
        width: 0.8em;
        height: 0.8em;
    }
`;
const SLine = styled.div`
    ${position("absolute", "50%", null, null, "0")};
    transform: translateY(-50%);
    width: 100%;
    height: 1px;
    border-top: 1px solid var(--grey-light4);
    transition: all 0.2s;
    opacity: 0;
    ${SAdd}:hover & {
        border-top: 1px solid var(--violet);
    }
    ${SAdd}.editable & {
        opacity: 1;
    }
`;

export default AdminSettingsPropertyAdd;
