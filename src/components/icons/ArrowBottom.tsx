import React, { Component } from "react";

export default class ArrowBottom extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                viewBox="0 0 50 50"
            >
                <path
                    fill="currentColor"
                    d="M 4.84375 12.90625 L 2.75 15 L 25 37.25 L 47.25 15 L 45.15625 12.90625 L 25 33.0625 Z "
                ></path>
            </svg>
        );
    }
}
