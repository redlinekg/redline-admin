import React, { PureComponent } from "react";

interface IconProps {
    name: string;
}

class Icon extends PureComponent<IconProps> {
    render() {
        const { name } = this.props;
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
            >
                <use xlinkHref={`#${name}`} />
            </svg>
        );
    }
}

export default Icon;
