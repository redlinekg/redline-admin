import React, { Component } from "react";
import {
    BlogIcon,
    ServiceIcon,
    SettingIcon,
    TeamIcon,
    BuildingIcon,
    IndexIcon,
    MapIcon,
    PageIcon,
    PortfolioIcon,
    CountriesIcon,
    GuideIcon,
    OrdersIcon,
    //
    ProductIcon,
    RewiewIcon,
    CategoryIcon,
    SlideIcon,
    AuthorIcon,
    UserIcon,
    ArticleIcon,
    CategoryArticleIcon,
    ContactIcon,
    BannerIcon,
    OfferIcon,
    PartnerIcon,
    PlaceIcon,
    RoomIcon,
    ConferenceroomIcon,
    EquipmentIcon,
    AnnouncementIcon,
    QuestionIcon,
    DocumentsIcon,
} from ".";

export default class IconsSymbol extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                style={{ display: "none" }}
            >
                <BlogIcon />
                <ServiceIcon />
                <SettingIcon />
                <TeamIcon />
                <BuildingIcon />
                <IndexIcon />
                <MapIcon />
                <PageIcon />
                <PortfolioIcon />
                <CountriesIcon />
                <GuideIcon />
                <OrdersIcon />

                <ProductIcon />
                <RewiewIcon />
                <CategoryIcon />
                <SlideIcon />
                <AuthorIcon />
                <UserIcon />
                <ArticleIcon />
                <CategoryArticleIcon />
                <ContactIcon />
                <BannerIcon />
                <OfferIcon />
                <PartnerIcon />
                <PlaceIcon />
                <RoomIcon />
                <ConferenceroomIcon />
                <EquipmentIcon />
                <AnnouncementIcon />
                <QuestionIcon />
                <DocumentsIcon />
            </svg>
        );
    }
}
