import React, { Component } from "react";

export default class SettingIcon extends Component {
    render() {
        return (
            <symbol id="SettingIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M59 8H15m11.999 24H7m52 0H38.999M51 56H7"
                ></path>
                <circle
                    cx="9"
                    cy="8"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <circle
                    cx="57"
                    cy="56"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <circle
                    cx="33"
                    cy="32"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
            </symbol>
        );
    }
}
