import React, { Component } from "react";

export default class UserIcon extends Component {
    render() {
        return (
            <symbol id="UserIcon" viewBox="0 0 64 64">
                <path
                    d="M52 10v1a5 5 0 0 1-10 0v-1H22v1a5 5 0 1 1-10 0v-1H2v44h60V10z"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M38 26h16m-16 8h16m-16 8h8"
                ></path>
                <circle
                    cx="20"
                    cy="28"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <path
                    d="M20 34a10.1 10.1 0 0 0-10 10.3V46h20v-1.7A10.1 10.1 0 0 0 20 34z"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
            </symbol>
        );
    }
}
