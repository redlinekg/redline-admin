import React, { Component } from "react";

export default class DocumentsIcon extends Component {
    render() {
        return (
            <symbol id="DocumentsIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M24 14H6v28"
                />
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M58 42V2H24v46m26-38H38m12 8H32m18 8H32m18 8h-8"
                />
                <path
                    d="M18 42v3a3 3 0 0 0 3 3h22a3 3 0 0 0 3-3v-3h16v20H2V42z"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                />
            </symbol>
        );
    }
}
