import React, { Component } from "react";

export default class OrdersIcon extends Component {
    render() {
        return (
            <symbol id="OrdersIcon" viewBox="0 0 64 64">
                <circle
                    cx="23"
                    cy="56"
                    r="4"
                    fill="none"
                    strokeWidth="4"
                    strokeMiterlimit="10"
                    stroke="currentColor"
                ></circle>
                <circle
                    cx="49"
                    cy="56"
                    r="4"
                    fill="none"
                    strokeWidth="4"
                    strokeMiterlimit="10"
                    stroke="currentColor"
                ></circle>
                <path
                    d="M3 4h8l4 16h46l-8 24H23a4 4 0 0 0-4 4 4 4 0 0 0 4 4h32"
                    fill="none"
                    strokeWidth="4"
                    strokeMiterlimit="10"
                    stroke="currentColor"
                ></path>
            </symbol>
        );
    }
}
