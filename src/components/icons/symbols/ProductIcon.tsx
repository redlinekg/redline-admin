import React, { Component } from "react";

export default class ProductIcon extends Component {
    render() {
        return (
            <symbol id="ProductIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M62 15.3L32 2 2 15.3v33.4L32 62l30-13.3V15.3z"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M2 15.3l30 13.3 30-13.3M18.4 8l30 13.3M32 28.6V62"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M54 31.6l-12 5.3v11.3l12-5.3V31.6z"
                ></path>
            </symbol>
        );
    }
}
