import React, { Component } from "react";

export default class BlogIcon extends Component {
    render() {
        return (
            <symbol id="BlogIcon" viewBox="0 0 64 64">
                <path
                    d="M20 38c0 6 0 14 10 18"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    d="M20 18v20"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    d="M56 34a6 6 0 1 0 0-12"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    d="M22 38H8a6 6 0 0 1-6-6v-8a6 6 0 0 1 6-6h18A55 55 0 0 0 56 8v40a50.4 50.4 0 0 0-30-10z"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
            </symbol>
        );
    }
}
