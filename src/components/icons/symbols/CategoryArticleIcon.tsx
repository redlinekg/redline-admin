import React, { Component } from "react";

export default class CategoryArticleIcon extends Component {
    render() {
        return (
            <symbol id="CategoryArticleIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M25.6 61L3 38.4 38.4 3l21.2 1.4L61 25.6 25.6 61z"
                ></path>
                <circle
                    cx="48"
                    cy="15"
                    r="4"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M31.3 21.4l11.3 11.3m-22.6 0l8.5 8.5M25.6 27l5.7 5.7"
                ></path>
            </symbol>
        );
    }
}
