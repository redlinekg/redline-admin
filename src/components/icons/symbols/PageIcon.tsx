import React, { Component } from "react";

export default class PageIcon extends Component {
    render() {
        return (
            <symbol id="PageIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="4"
                    d="M2 8h60v12H2zm0 12v36h60V20"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="4"
                    d="M26 30h24m-36 0h4m8 8h24m-36 0h4m8 8h24m-36 0h4"
                ></path>
                <circle
                    cx="8"
                    cy="14"
                    r="1"
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="4"
                ></circle>
                <circle
                    cx="15"
                    cy="14"
                    r="1"
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="4"
                ></circle>
                <circle
                    cx="22"
                    cy="14"
                    r="1"
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="4"
                ></circle>
            </symbol>
        );
    }
}
