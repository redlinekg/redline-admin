import React, { Component } from "react";

export default class SlideIcon extends Component {
    render() {
        return (
            <symbol id="SlideIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M46 46v10H2V28h8"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M54 36v10H10V18h8"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M18 8h44v28H18z"
                ></path>
            </symbol>
        );
    }
}
