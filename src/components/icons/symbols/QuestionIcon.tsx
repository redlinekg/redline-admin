import React, { Component } from "react";

export default class QuestionIcon extends Component {
    render() {
        return (
            <symbol id="QuestionIcon" viewBox="0 0 64 64">
                <circle
                    data-name="layer2"
                    cx="32"
                    cy="32"
                    r="30"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="3"
                />
                <path
                    data-name="layer1"
                    d="M24 24.528C24 21.647 26.206 17 32.47 17S40 22.628 40 24.528s-.59 4.092-4.057 7.558C32.477 35.552 32 37.353 32 39v2"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="3"
                ></path>
                <circle
                    data-name="layer1"
                    cx="32"
                    cy="47"
                    r="2"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="3"
                />
            </symbol>
        );
    }
}
