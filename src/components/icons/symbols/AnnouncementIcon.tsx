import React, { Component } from "react";

export default class AnnouncementIcon extends Component {
    render() {
        return (
            <symbol id="AnnouncementIcon" viewBox="0 0 64 64">
                ]
                <path
                    d="M32 45.2V47a9 9 0 0 1-9 9 9 9 0 0 1-9-9v-7.2"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="3"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="3"
                    d="M2 24v16M62 6v52M2 28l60-18M2 36l60 18"
                ></path>
            </symbol>
        );
    }
}
