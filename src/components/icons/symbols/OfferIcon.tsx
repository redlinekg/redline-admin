import React, { Component } from "react";

export default class OfferIcon extends Component {
    render() {
        return (
            <symbol id="OfferIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M2 2h60v12H2zm0 24h60v12H2z"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M2 50h60v12H2z"
                ></path>
            </symbol>
        );
    }
}
