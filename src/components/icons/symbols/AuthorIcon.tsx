import React, { Component } from "react";

export default class AuthorIcon extends Component {
    render() {
        return (
            <symbol id="AuthorIcon" viewBox="0 0 64 64">
                <path
                    d="M36 51H2s0-7 9.4-8.8S20 38.6 20 37v-2a14.2 14.2 0 0 1-4-7c-2.5 0-4-3-4-6 0-.8 0-4 2-4-1.6-6.4-.4-13 4-13 10.4-7 25-1.5 20 13 2 0 2 3.2 2 4 0 3-1.5 6-4 6a14.1 14.1 0 0 1-4 7v2c0 1.1.5 3 3.9 4l8.1 2"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M56 31L35.2 51.7 30.8 62l10.4-4.3L62 37l-6-6zM35.8 51.5l6 6"
                ></path>
            </symbol>
        );
    }
}
