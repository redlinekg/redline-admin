import React, { Component } from "react";

export default class RoomIcon extends Component {
    render() {
        return (
            <symbol id="RoomIcon" viewBox="0 0 64 64">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <path
                        d="M46 32V17a15 15 0 0 0-30 0 4 4 0 0 0 8 .1 7 7 0 1 1 12.2 4.7C30.7 27.1 16 26.2 16 36v24a2 2 0 0 0 2 2h26a2 2 0 0 0 2-2V32z"
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="4"
                    ></path>
                    <path
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="4"
                        d="M24 36h14v18H24z"
                    ></path>
                </svg>
            </symbol>
        );
    }
}
