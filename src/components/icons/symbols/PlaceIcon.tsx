import React, { Component } from "react";

export default class PlaceIcon extends Component {
    render() {
        return (
            <symbol id="PlaceIcon" viewBox="0 0 64 64">
                <path
                    d="M33 46v16m0-42v8"
                    strokeWidth="4"
                    strokeMiterlimit="10"
                    stroke="currentColor"
                    fill="none"
                ></path>
                <path
                    d="M51 20H15V2h36l6 9-6 9zM15 46h36V28H15l-6 9 6 9z"
                    strokeWidth="4"
                    strokeMiterlimit="10"
                    stroke="currentColor"
                    fill="none"
                ></path>
            </symbol>
        );
    }
}
