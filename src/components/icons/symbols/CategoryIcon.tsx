import React, { Component } from "react";

export default class CategoryIcon extends Component {
    render() {
        return (
            <symbol id="CategoryIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M26 6.012h36v8H26zm0 44h36v8H26zM26 28h36v8H26z"
                ></path>
                <circle
                    cx="8"
                    cy="10"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <circle
                    cx="8"
                    cy="54"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <circle
                    cx="8"
                    cy="32"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
            </symbol>
        );
    }
}
