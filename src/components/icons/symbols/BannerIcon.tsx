import React, { Component } from "react";

export default class BannerIcon extends Component {
    render() {
        return (
            <symbol id="BannerIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M14 18h36v28H14z"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M8 18H2m6 28H2m60-28h-6M50 6v6M14 6v6m36 40v6m-36-6v6m48-12h-6"
                ></path>
            </symbol>
        );
    }
}
