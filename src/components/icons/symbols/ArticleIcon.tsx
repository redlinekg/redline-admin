import React, { Component } from "react";

export default class ArticleIcon extends Component {
    render() {
        return (
            <symbol id="ArticleIcon" viewBox="0 0 64 64">
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M8 2h48v60H8zm12 0v8m-2 0h4m10-8v8m-2 0h4m10-8v8m-2 0h4"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M18 20h28M18 30h28M18 40h28M18 50h20"
                ></path>
            </symbol>
        );
    }
}
