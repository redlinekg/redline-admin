import React, { Component } from "react";

export default class EquipmentIcon extends Component {
    render() {
        return (
            <symbol id="EquipmentIcon" viewBox="0 0 64 64">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <path
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="3"
                        d="M10 4v32M54 4v32M10 56v4m44-4v4"
                    ></path>
                    <rect
                        x="2"
                        y="36"
                        width="60"
                        height="20"
                        rx="2"
                        ry="2"
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="3"
                    ></rect>
                    <path
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="3"
                        d="M10 46h18"
                    ></path>
                    <circle
                        cx="51"
                        cy="45"
                        r="2"
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="3"
                    ></circle>
                </svg>
            </symbol>
        );
    }
}
