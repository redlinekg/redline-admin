import React, { Component } from "react";

export default class RewiewIcon extends Component {
    render() {
        return (
            <symbol id="RewiewIcon" viewBox="0 0 64 64">
                <path
                    d="M24 52a8 8 0 0 1 8 8V16a8 8 0 0 0-8-8H2v44zM54 8h8v44H40a8 8 0 0 0-8 8"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    d="M32 16a8 8 0 0 1 8-8h2"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M54 26l-6-4-6 4V4h12v22z"
                ></path>
            </symbol>
        );
    }
}
