import React, { Component } from "react";

export default class ConferenceroomIcon extends Component {
    render() {
        return (
            <symbol id="ConferenceroomIcon" viewBox="0 0 64 64">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <path
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                        strokeWidth="4"
                        d="M32 42v20m-8 0h16"
                    ></path>
                    <path
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                        strokeWidth="4"
                        d="M60 42L48 18H16L4 42m-2 0h60"
                    ></path>
                    <path
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                        strokeWidth="4"
                        d="M44 20V8.1L40 6"
                    ></path>
                    <path
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                        strokeWidth="4"
                        d="M38 24H20l-4.8 10H42l-4-10z"
                    ></path>
                    <path
                        d="M40 8V4a2 2 0 0 0-2-2h-8a2 2 0 0 0-2 2v4z"
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                        strokeWidth="4"
                    ></path>
                </svg>
            </symbol>
        );
    }
}
