import React, { Component } from "react";

export default class PartnerIcon extends Component {
    render() {
        return (
            <symbol id="PartnerIcon" viewBox="0 0 64 64">
                <circle
                    cx="32"
                    cy="39"
                    r="7"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <path
                    d="M32 46a12.1 12.1 0 0 0-12 12v2h24v-2a12.1 12.1 0 0 0-12-12z"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <circle
                    cx="52"
                    cy="10"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <path
                    d="M62 28c0-7.5-4.5-12-10-12s-10 4.5-10 12z"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <circle
                    cx="12"
                    cy="10"
                    r="6"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></circle>
                <path
                    d="M22 28c0-7.5-4.5-12-10-12S2 20.5 2 28z"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                ></path>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="4"
                    d="M12 34l8 8m32-8l-8 8M24 14h16"
                ></path>
            </symbol>
        );
    }
}
