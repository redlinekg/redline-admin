import React, { ReactElement } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import Button, { ButtonProps } from "antd/es/button";
import Popconfirm, { PopconfirmProps } from "antd/es/popconfirm";

export interface AdminButtonConfirmationProps {
    onClick: () => {};
    button_type: ButtonProps["type"];
    children: ReactElement;
    popconfirm_placement: PopconfirmProps["placement"];
}

const AdminButtonConfirmation = ({
    onClick,
    button_type,
    children,
    popconfirm_placement,
}: AdminButtonConfirmationProps) => (
    <SAdminButtonConfirmation className="SAdminButtonConfirmation">
        <Popconfirm
            title="Вы действительно хотите удалить?"
            okText="Да"
            cancelText="Нет"
            placement={popconfirm_placement}
            onConfirm={onClick}
        >
            <Button type={button_type}>{children}</Button>
        </Popconfirm>
    </SAdminButtonConfirmation>
);

const SAdminButtonConfirmation = styled.div``;

AdminButtonConfirmation.defaultProps = {
    popconfirm_placement: "topLeft",
};

AdminButtonConfirmation.propTypes = {
    onClick: PT.func,
    button_type: PT.string,
    children: PT.any,
    popconfirm_placement: PT.string,
};

export default AdminButtonConfirmation;
