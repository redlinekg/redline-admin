import React from "react";
import { isBrowser } from "browser-or-node";
import PT from "prop-types";
import { Portal } from "react-portal";

class AdminHeaderButtonsPortal extends React.Component {
    static propTypes = {
        children: PT.any,
    };
    render() {
        const { children } = this.props;
        return isBrowser ? (
            <Portal
                node={document && document.getElementById("header_buttons")}
            >
                {children}
            </Portal>
        ) : null;
    }
}

export default AdminHeaderButtonsPortal;
