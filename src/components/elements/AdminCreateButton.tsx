import Button from "antd/es/button";
import PT from "prop-types";
import React, { FunctionComponent } from "react";
import Link from "./Link";
import { RouteParams } from "../../interfaces";

export interface AdminCreateButtonProps {
    route: string;
    params?: RouteParams;
}

const AdminCreateButton: FunctionComponent<AdminCreateButtonProps> = ({
    route,
    params,
}: AdminCreateButtonProps) => (
    <Link route={route} params={params || { id: "_" }}>
        <Button type="primary">Добавить</Button>
    </Link>
);

AdminCreateButton.propTypes = {
    route: PT.string.isRequired,
    params: PT.objectOf(PT.string.isRequired).isRequired,
};

export default AdminCreateButton;
