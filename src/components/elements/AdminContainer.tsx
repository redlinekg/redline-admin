import React, { PureComponent } from "react";
import { styled } from "linaria/react";
import PT from "prop-types";

export default class Container extends PureComponent {
    static propTypes = {
        children: PT.any.isRequired,
    };

    render() {
        const { children } = this.props;
        return <SContainer>{children}</SContainer>;
    }
}

const SContainer = styled.div`
    padding: 0 var(--global-padding-content);
    &.left {
        padding: 0 0 0 var(--global-padding-content);
    }
    &.right {
        padding: 0 var(--global-padding-content) 0 0;
    }
`;
