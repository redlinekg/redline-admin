import Button, { ButtonProps } from "antd/es/button";
import Popconfirm from "antd/es/popconfirm";
import PT from "prop-types";
import React, { useContext, FunctionComponent } from "react";
import { AdminFormContext } from "../form/AdminForm";

export interface DeleteButtonProps {
    onClick?: ((e: any) => void) | undefined;
}

const DeleteButton: FunctionComponent<DeleteButtonProps> = ({
    onClick,
    ...rest
}) => {
    const { handleDelete } = useContext(AdminFormContext);
    return (
        <Popconfirm
            title={"Вы действительно хотите удалить?"}
            okText="Да"
            cancelText="Нет"
            placement="topLeft"
            onConfirm={onClick || handleDelete}
        >
            <Button type="danger" {...rest}>
                Удалить
            </Button>
        </Popconfirm>
    );
};

DeleteButton.propTypes = {
    onClick: PT.func,
};

export default DeleteButton;
