import cn from "classnames";
import PT from "prop-types";
import React, { cloneElement, ReactElement, useContext } from "react";
import RoutesContext from "../../contexts/RoutesContext";
import { RouteParams } from "../../interfaces";

export interface ActiveLinkProps {
    children: ReactElement;
    route: string;
    params?: RouteParams;
}

const ActiveLink = ({ children, route, params, ...props }: ActiveLinkProps) => {
    const {
        routes: { Link },
        routes,
    } = useContext(RoutesContext);
    const active =
        route === undefined
            ? false
            : routes.findAndGetUrls(route, params).urls.as === routes.asPath;
    const className = cn(children.props.className, {
        active,
    });

    return (
        <Link route={route} params={params} {...props}>
            {cloneElement(children, { className })}
        </Link>
    );
};
ActiveLink.propTypes = {
    routes: PT.object,
    route: PT.string,
    params: PT.object,
    children: PT.any,
};

export default ActiveLink;
