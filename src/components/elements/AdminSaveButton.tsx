import Button, { ButtonProps } from "antd/es/button";
import { connect } from "formik";
import React from "react";

export interface SaveButtonProps extends ButtonProps {
    editable: boolean;
}

export const AdminSaveButton = connect<SaveButtonProps>(
    ({
        editable = false,
        onClick,
        formik: { handleSubmit, errors },
        children,
        ...rest
    }) => {
        return (
            <Button
                type="primary"
                disabled={Object.keys(errors).length > 0}
                onClick={onClick || (handleSubmit as any)}
                {...rest}
            >
                {children ? children : editable ? "Сохранить" : "Добавить"}
            </Button>
        );
    }
);

export default AdminSaveButton;
