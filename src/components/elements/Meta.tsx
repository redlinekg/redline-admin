import React, { Component } from "react";
import { Helmet } from "react-helmet";
import Head from "next/head";
import PT from "prop-types";

export interface MetaProps {
    title: string;
    part: string;
}

class Meta extends Component<MetaProps> {
    static defaultProps = {
        part: " - Админ панель",
    };

    static propTypes = {
        title: PT.string.isRequired,
        part: PT.string,
    };

    render() {
        const { title, part } = this.props;
        return (
            <>
                <Head>
                    <title>{`${title ? title : "Загрузка"} ${part}`}</title>
                </Head>
                <Helmet
                    meta={[
                        {
                            property: "og:type",
                            content: "website",
                        },
                        {
                            property: "og:site_name",
                            content: "ADMIN",
                        },
                    ]}
                />
            </>
        );
    }
}

export default Meta;
