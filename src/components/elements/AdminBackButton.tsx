import React, { Component } from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import Button, { ButtonProps } from "antd/es/button";

import Icon from "antd/es/icon";
import { down } from "../../config/vars";

interface AdminBackButtonProps extends ButtonProps {
    router: any;
}

class AdminBackButton extends Component<AdminBackButtonProps> {
    static propTypes = {
        router: PT.object,
    };

    back = () => {
        const { router } = this.props;
        if (!router) {
            throw new Error("'router' in AdminBackButton is not defined");
        }
        router.back();
    };

    render() {
        return (
            <Button onClick={this.back}>
                <SBlock>
                    <SIcon>
                        <Icon type="left" />
                    </SIcon>
                    <SText>Назад</SText>
                </SBlock>
            </Button>
        );
    }
}

const SBlock = styled.div`
    display: inline-flex;
`;
const SIcon = styled.div``;
const SText = styled.div`
    margin-left: 0.4em;
    ${down("lg")} {
        display: none;
    }
`;

export default AdminBackButton;
