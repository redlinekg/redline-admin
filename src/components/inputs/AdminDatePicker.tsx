import React from "react";
import { DatePicker } from "formik-antd";
import { styled } from "linaria/react";
import Form from "antd/es/form";
import { connect } from "formik";
import get from "lodash/get";

import {
    AdminInputProps,
    FormikAdminInputProps,
    AdminInputChangeEvent,
} from "./interfaces";

const dateFormat = "YYYY-MM-DD HH:mm:ss";

export interface AdminDatePickerProps
    extends AdminInputProps,
        AdminInputChangeEvent {}

class AdminDatePicker extends React.PureComponent<
    AdminDatePickerProps & FormikAdminInputProps
> {
    render() {
        const {
            label,
            name,
            translate_lang,
            formik: { errors },
            ...rest
        } = this.props;
        translate_lang;
        const error = get(errors, name);
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <SAdminDatePicker>
                    <DatePicker
                        name={name}
                        allowClear={false}
                        showTime={true}
                        format={dateFormat}
                        fast={true}
                        {...rest}
                    />
                </SAdminDatePicker>
            </Form.Item>
        );
    }
}

const SAdminDatePicker = styled.div`
    .ant-calendar-picker {
        width: 100%;
    }
`;

export default connect(AdminDatePicker);
