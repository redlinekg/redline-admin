import Form from "antd/es/form";
import { connect } from "formik";
import { Input } from "formik-antd";
import get from "lodash/get";
import React from "react";
import {
    AdminInputChangeEvent,
    AdminInputProps,
    FormikAdminInputProps,
} from "./interfaces";

export interface AdminTextareaProps
    extends AdminInputProps,
        AdminInputChangeEvent {}

class AdminTextarea extends React.PureComponent<
    AdminTextareaProps & FormikAdminInputProps
> {
    // static propTypes = {
    //     ...form_item_props,
    //     ...admin_input_props
    // };
    render() {
        const {
            label,
            name,
            formik: { errors },
            translate_lang,
            ...rest
        } = this.props;
        translate_lang;
        const error = get(errors, name);
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <Input.TextArea name={name} fast={true} {...rest} rows={4} />
            </Form.Item>
        );
    }
}

export default connect(AdminTextarea);
