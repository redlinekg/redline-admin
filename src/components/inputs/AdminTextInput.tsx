import React, { PureComponent } from "react";
import { Input } from "formik-antd";
import Form from "antd/es/form";
import { connect } from "formik";
import get from "lodash/get";
import { styled } from "linaria/react";

import {
    AdminInputProps,
    FormikAdminInputProps,
    AdminInputChangeEvent,
} from "./interfaces";

export interface AdminTextInputProps
    extends AdminInputProps,
        AdminInputChangeEvent {}

class AdminTextInput extends PureComponent<
    AdminTextInputProps & FormikAdminInputProps
> {
    render() {
        const {
            help,
            extra,
            label,
            name,
            formik: { errors },
            translate_lang,
            ...rest
        } = this.props;
        translate_lang;
        const error = get(errors, name);
        return (
            <Form.Item
                label={
                    <SLabel>
                        {label}
                        {help ? <SHelp>{help}</SHelp> : null}
                    </SLabel>
                }
                colon={help ? false : true}
                extra={extra}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <Input name={name} {...rest} />
            </Form.Item>
        );
    }
}

const SLabel = styled.div`
    display: inline-block;
    line-height: 1.3;
`;

const SHelp = styled.div`
    color: var(--grey);
    white-space: normal;
    font-size: 0.9em;
`;

export default connect(AdminTextInput);
