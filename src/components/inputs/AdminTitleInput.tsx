import React from "react";
import { connect } from "formik";
import { styled } from "linaria/react";
import slugify from "slugify";
import AdminTextInput from "./AdminTextInput";
import { AdminInputProps, FormikAdminInputProps } from "./interfaces";

export interface AdminTitleInputProps extends AdminInputProps {
    slug_name: string;
    slug_label: string;
    autoslug: boolean;
}

class AdminTitleInput extends React.PureComponent<
    AdminTitleInputProps & FormikAdminInputProps
> {
    state = {
        slug_has_changed: false,
    };

    onTitleChange = (e: any) => {
        const {
            name,
            slug_name,
            formik: { values: old_values, setValues },
            translate_lang,
            autoslug,
        } = this.props;
        const { slug_has_changed } = this.state;
        const value = e.target.value;
        const values = {
            [name]: value,
        };
        /*
         * Автоматическое заполнение слага отключено если
         * поле находится в AdminTranslateTab или если не включена опция autoslug
         */
        if (autoslug && !slug_has_changed && !translate_lang) {
            values[slug_name] = slugify(value).toLowerCase();
        }
        setValues({ ...old_values, ...values });
    };

    onSlugChange = (e: any) => {
        const {
            slug_name,
            formik: { setFieldValue },
        } = this.props;
        this.setState({
            slug_has_changed: true,
        });
        setFieldValue(slug_name, e.target.value);
    };

    render() {
        const {
            label,
            name,
            slug_label,
            slug_name,
            autoslug,
            ...rest
        } = this.props;
        autoslug;
        return (
            <>
                <AdminTextInput
                    name={name}
                    label={label}
                    onChange={this.onTitleChange}
                    {...rest}
                />
                {!rest.translate_lang ? (
                    <SAdmitTextInputSlug>
                        <AdminTextInput
                            name={slug_name}
                            label={slug_label}
                            onChange={this.onSlugChange}
                            {...rest}
                        />
                    </SAdmitTextInputSlug>
                ) : null}
            </>
        );
    }
}

const SAdmitTextInputSlug = styled.div`
    .ant-tabs-tabpane-active & {
        .ant-form-item-control.has-error .ant-form-explain {
            display: block !important;
            text-align: right;
        }
    }
`;

export default connect(AdminTitleInput);
