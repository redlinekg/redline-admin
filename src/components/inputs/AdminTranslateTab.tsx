import React, { PureComponent, ReactElement } from "react";
import { styled } from "linaria/react";
import { Tabs } from "antd";
import { connect } from "formik";
import get from "lodash/get";

import { Language } from "../../interfaces";
import { AdminInputProps, FormikAdminInputProps } from "./interfaces";

const { TabPane } = Tabs;

export interface AdminTranslateTabProps extends AdminInputProps {
    delimiter?: string;
    languages: Language[];
    default_language?: string;
    name: string;
    children: ReactElement;
    errors: {};
}

export interface AdminTranslateTabDefaultProps {
    default_language: string;
    delimiter: string;
}

class AdminTranslateTab extends PureComponent<
    AdminTranslateTabProps & FormikAdminInputProps
> {
    static defaultProps: AdminTranslateTabDefaultProps = {
        default_language: "ru",
        delimiter: "__",
    };

    // static propTypes = {
    //     delimiter: PT.string,
    //     languages: PT.array,
    //     default_language: PT.string,
    //     name: PT.string,
    //     children: PT.any,
    //     errors: PT.object,
    //     formik: PT.object
    // };
    getErrors = () => {
        const {
            name,
            languages,
            default_language,
            delimiter,
            formik: { errors },
        } = this.props as AdminTranslateTabProps &
            AdminTranslateTabDefaultProps &
            FormikAdminInputProps;
        return languages.map((lang) => {
            let error = null;
            if (lang.code === default_language) {
                error = get(errors, name);
            } else {
                error = get(errors, `${name}${delimiter}${lang.code}`);
            }
            if (error) {
                error = `${error} [${lang.code}]`;
            }
            return error;
        });
    };
    render() {
        const {
            languages,
            name,
            default_language,
            children,
            delimiter,
        } = this.props;
        const errors = this.getErrors();
        return (
            <SAdminTranslateTab>
                <Tabs defaultActiveKey="0" size="small" animated={false}>
                    {languages.map((l, idx) => (
                        <TabPane key={idx.toString()} tab={l.name}>
                            {React.cloneElement(children, {
                                name:
                                    l.code === default_language
                                        ? name
                                        : `${name}${delimiter}${l.code}`,
                                translate_lang:
                                    l.code !== default_language ? l.code : null,
                                key: idx,
                            })}
                        </TabPane>
                    ))}
                </Tabs>
                {errors && errors.length
                    ? errors.map((error, idx) => (
                          <SErrors key={idx}>{error}</SErrors>
                      ))
                    : null}
            </SAdminTranslateTab>
        );
    }
}

const SAdminTranslateTab = styled.div`
    margin-bottom: 25px;
    .ant-tabs {
        .ant-tabs-nav-scroll {
            display: flex;
            justify-content: flex-end;
        }
        .ant-tabs-bar {
            margin: 0 0 3px;
            border-bottom: 0;
        }
        .ant-tabs-small-bar {
            .ant-tabs-tab {
                padding: 3px 11px;
                margin: 0 12px 0 0;
                &:last-of-type {
                    margin-right: 0;
                }
                &:before {
                    display: none;
                }
            }
        }
        .ant-tabs-nav .ant-tabs-tab-active {
            color: var(--black);
            background-color: var(--grey-light3);
        }
        .ant-tabs-ink-bar {
            display: none !important;
        }
        /* hide error */
        .ant-form-explain {
            display: none !important;
        }
        .ant-row {
            margin-bottom: 0 !important;
        }
    }
`;
const SErrors = styled.div`
    color: rgba(0, 0, 0, 0.45);
    font-size: 0.96em;
    line-height: 1.5;
    color: var(--red);
    text-align: right;
`;

export default connect(AdminTranslateTab);
