import { FormikContextType } from "formik";

export interface AdminInputProps {
    name: string;
    label: string;
    help: string;
    extra: string;
    translate_lang: string | undefined;
}

export interface AdminInputChangeValue {
    onChange: (v: any) => void;
}

export interface AdminInputChangeEvent {
    onChange: (v: any) => void;
}

export interface FormikAdminInputProps {
    formik: FormikContextType<any>;
}

// selects

export type VariantValuePreparer = (v: any) => any;
export type ValuePreparer = (v: any) => any;
export type ValueConverter = (v: any) => any;

export interface SelectVariant {
    value: any;
    label: string;
    children: SelectVariant[] | undefined;
}

// uploads

export type UrlExecutor = (response_body: any) => string;
export type UploadType = "image" | "file";
export type MimeType = string;
export type MimeTypes = MimeType[];
export type UploadValuesConverter = (
    v: any
) => {
    uid: string;
    name: string;
    status: string;
    url: string;
};
