import { Radio } from "formik-antd";
import Form from "antd/es/form";
import { connect } from "formik";
import get from "lodash/get";
import PT from "prop-types";
import React from "react";
import {
    AdminInputProps,
    FormikAdminInputProps,
    AdminInputChangeEvent,
    SelectVariant,
} from "./interfaces";

export interface AdminRadioProps
    extends AdminInputProps,
        AdminInputChangeEvent {
    variants: SelectVariant[];
}

class AdminRadio extends React.PureComponent<
    AdminRadioProps & FormikAdminInputProps
> {
    // static propTypes = {
    //     variants: PT.arrayOf(
    //         PT.shape({
    //             value: PT.string,
    //             label: PT.string
    //         })
    //     ),
    //     ...form_item_props,
    //     ...admin_input_props
    // };
    render() {
        const {
            variants,
            label,
            name,
            formik: { errors },
            translate_lang,
            ...rest
        } = this.props;
        translate_lang;
        const error = get(errors, name);
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <Radio.Group
                    name={name}
                    buttonStyle="solid"
                    fast={true}
                    {...rest}
                >
                    {variants.map((v, idx) => (
                        <Radio.Button key={idx} value={v.value}>
                            {v.label}
                        </Radio.Button>
                    ))}
                </Radio.Group>
            </Form.Item>
        );
    }
}

export default connect(AdminRadio);
