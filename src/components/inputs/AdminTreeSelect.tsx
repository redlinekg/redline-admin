import Form from "antd/es/form";
import TreeSelect from "antd/es/tree-select";
import { connect } from "formik";
import get from "lodash/get";
import React, { ReactElement } from "react";
import {
    AdminInputProps,
    FormikAdminInputProps,
    SelectVariant,
    ValueConverter,
    ValuePreparer,
    VariantValuePreparer,
} from "./interfaces";

export interface AdminTreeSelectProps extends AdminInputProps {
    variants: SelectVariant[];
    validate?: () => {};
    onChange: (...args: any[]) => {};
    prepareVariantValue?: VariantValuePreparer;
    prepareValue?: ValuePreparer;
    convertValue?: ValueConverter;
}

export interface AdminTreeSelectDefaultProps {
    prepareVariantValue: VariantValuePreparer;
    prepareValue: ValuePreparer;
    convertValue: ValueConverter;
}

class AdminTreeSelect extends React.Component<
    AdminTreeSelectProps & FormikAdminInputProps
> {
    static defaultProps: AdminTreeSelectDefaultProps = {
        prepareVariantValue: (v: any) => v,
        prepareValue: (v: any) => v,
        convertValue: (v: any) => v,
    };

    // static propTypes = {
    //     variants: PT.arrayOf(
    //         PT.shape({
    //             value: PT.any,
    //             label: PT.string,
    //             children: PT.array
    //         })
    //     ),
    //     validate: PT.any,
    //     onChange: PT.func,
    //     prepareVariantValue: PT.func,
    //     prepareValue: PT.func, //
    //     convertValue: PT.func, // change prepared value to original value
    //     ...form_item_props,
    //     ...admin_input_props
    // };

    renderNodes(variants?: SelectVariant[]): ReactElement[] {
        const { prepareVariantValue } = this.props as AdminTreeSelectProps &
            AdminTreeSelectDefaultProps;
        if (!variants) {
            variants = this.props.variants;
        }

        return variants.map((variant) => (
            <TreeSelect.TreeNode
                value={prepareVariantValue(variant.value)}
                title={variant.label}
                key={variant.label}
            >
                {variant.children && variant.children.length
                    ? this.renderNodes(variant.children)
                    : null}
            </TreeSelect.TreeNode>
        ));
    }

    render() {
        const {
            prepareValue,
            convertValue,
            label,
            name,
            formik: { errors, values, setFieldValue, setFieldTouched },
            onChange,
            translate_lang,
            ...rest
        } = this.props as AdminTreeSelectProps &
            AdminTreeSelectDefaultProps &
            FormikAdminInputProps;
        translate_lang;
        const error = get(errors, name);
        const value = get(values, name);
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <TreeSelect
                    onChange={(value, option) => {
                        value = convertValue(value);
                        setFieldValue(name, value);
                        onChange && onChange(value, option);
                    }}
                    onBlur={() => setFieldTouched(name)}
                    value={prepareValue(value)}
                    {...rest}
                >
                    {this.renderNodes()}
                </TreeSelect>
            </Form.Item>
        );
    }
}

export default connect(AdminTreeSelect);
