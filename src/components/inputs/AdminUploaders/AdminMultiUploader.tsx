import Button from "antd/es/button";
import Form from "antd/es/form";
import Icon from "antd/es/icon";
import message from "antd/es/message";
import Modal from "antd/es/modal";
import Upload from "antd/es/upload";
import { UploadFile, UploadChangeParam } from "antd/es/upload/interface";

import CN from "classnames";
import { connect } from "formik";
import get from "lodash/get";
import React, { PureComponent } from "react";
import { createFileValidator, getBase64, MIME_TYPES } from "./util";
import {
    AdminInputProps,
    FormikAdminInputProps,
    AdminInputChangeValue,
    UrlExecutor,
    MimeTypes,
    UploadType,
    UploadValuesConverter,
} from "../interfaces";

const uploadButtonFile = (
    <Button>
        <Icon type="upload" /> Добавить
    </Button>
);
const uploadButtonImage = (
    <div>
        <Icon type="file" /> Добавить
    </div>
);

export interface AdminMultiUploaderProps
    extends AdminInputProps,
        AdminInputChangeValue {
    upload_url: string;
    url_executor?: UrlExecutor;
    form_data_field?: string;
    allowed_types?: MimeTypes;
    size_limit?: number;
    type?: UploadType;
    convertValue: UploadValuesConverter;
}

export interface AdminMultiUploaderDefaultProps {
    url_executor: UrlExecutor;
    form_data_field: string;
    allowed_types: MimeTypes;
    size_limit: number;
    type: UploadType;
}

export interface AdminMultiUploaderState {
    previewVisible: boolean;
    previewImage: string;
    localFileList: UploadFile[];
}

class AdminMultiUploader extends PureComponent<
    AdminMultiUploaderProps & FormikAdminInputProps,
    AdminMultiUploaderState
> {
    static defaultProps: AdminMultiUploaderDefaultProps = {
        allowed_types: MIME_TYPES,
        url_executor: (response) => response.path,
        form_data_field: "file",
        size_limit: 1024 * 1024 * 2, // by default: 2MB
        type: "image",
    };

    state: AdminMultiUploaderState = {
        previewVisible: false,
        previewImage: "",
        localFileList: [],
    };

    getValue = () => {
        const {
            formik: { values },
            name,
        } = this.props as AdminMultiUploaderProps &
            AdminMultiUploaderDefaultProps &
            FormikAdminInputProps;
        const value = get(values, name);
        return value;
    };

    handleCancel = () => this.setState({ previewVisible: false });

    handlePreview = (async (file: UploadFile) => {
        if (!file.url && !(file as any).preview) {
            (file as any).preview = await getBase64(file.originFileObj as File);
        }

        this.setState({
            previewImage: file.url || (file as any).preview,
            previewVisible: true,
        });
    }) as (file: UploadFile) => void;

    handleChange = ({ file }: UploadChangeParam) => {
        const {
            formik: { setFieldValue },
            name,
            convertValue,
        } = this.props as AdminMultiUploaderProps &
            AdminMultiUploaderDefaultProps &
            FormikAdminInputProps;
        const { localFileList } = this.state;
        const value = this.getValue();
        const exist = localFileList.find((f) => f.uid === file.uid);
        switch (file.status) {
            case "uploading":
                if (!exist) {
                    this.setState({ localFileList: [...localFileList, file] });
                } else {
                    this.setState({
                        localFileList: localFileList.map((f) => {
                            if (f.uid === file.uid) {
                                return file;
                            }
                            return f;
                        }),
                    });
                }
                break;
            case "done":
                this.setState(
                    {
                        localFileList: localFileList.filter(
                            (f) => f.uid !== file.uid
                        ),
                    },
                    () => {
                        setFieldValue(name, [...(value || []), file.response]);
                    }
                );
                break;
            case "removed":
                this.setState(
                    {
                        localFileList: localFileList.filter(
                            (f) => f.uid + "" !== file.uid + ""
                        ),
                    },
                    () => {
                        const new_value = (value || [])
                            .map((v: any, idx: number): ReturnType<
                                UploadValuesConverter
                            > & { original: any } => {
                                return {
                                    ...convertValue(v),
                                    original: v,
                                };
                            })
                            .filter((f: any) => f.uid + "" !== file.uid + "")
                            .map((v: any) => v.original);
                        setFieldValue(name, new_value);
                    }
                );
                break;
            case "error":
                message.error("Произошла ошибка при загрузке файла");
                this.setState({
                    localFileList: localFileList.filter(
                        (f) => f.uid !== file.uid
                    ),
                });
                break;
            default:
                alert(`Strange status ${file.status}`);
                message.error("Произошла ошибка при загрузке файла");
        }
    };

    render() {
        const {
            formik: { errors },
            name,
            allowed_types,
            size_limit,
            upload_url,
            form_data_field,
            label,
            type,
            convertValue,
        } = this.props as AdminMultiUploaderProps &
            AdminMultiUploaderDefaultProps &
            FormikAdminInputProps;
        const { previewVisible, previewImage, localFileList } = this.state;
        const error = get(errors, name);
        const value = this.getValue();
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
                className="clearfix"
            >
                <Upload
                    name={form_data_field}
                    listType={type === "image" ? "picture-card" : "text"}
                    className={CN({
                        "avatar-uploader": type === "image",
                    })}
                    action={upload_url}
                    beforeUpload={createFileValidator(
                        allowed_types,
                        size_limit
                    )}
                    multiple={true}
                    onChange={this.handleChange}
                    onPreview={this.handlePreview}
                    // already uploaded files
                    fileList={[
                        ...(value || [])
                            .map(convertValue)
                            .map((v: any, idx: number) => ({
                                ...v,
                                original: value[idx],
                            })),
                        ...localFileList,
                    ]}
                >
                    <>
                        {type === "image"
                            ? uploadButtonImage
                            : uploadButtonFile}
                    </>
                </Upload>
                <Modal
                    visible={previewVisible}
                    footer={null}
                    onCancel={this.handleCancel}
                >
                    <img
                        alt="example"
                        style={{ width: "100%" }}
                        src={previewImage}
                    />
                </Modal>
            </Form.Item>
        );
    }
}

export default connect(AdminMultiUploader);
