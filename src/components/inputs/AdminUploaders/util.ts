import message from "antd/es/message";
import { MimeTypes } from "../interfaces";

const bytesToMegabytes = (bytes: number) => bytes / 1024 / 1024;

/**
 * create validator
 * @param {Array} allowed_types array of allowed file-types
 * @param {Number} size_limit max size of file
 */
export const createFileValidator = (
    allowed_types: MimeTypes,
    size_limit: number
) => (file: File) => {
    const isAllowedType = allowed_types.includes(file.type);
    if (!isAllowedType) {
        message.error(
            `Разрешены только избражений в формате SVG, JPG, PNG 2MB; MIME полученного файла:${file.type}`
        );
    }
    const isAllowedSize = file.size < size_limit;
    if (!isAllowedSize) {
        message.error(
            `Превышен размер загружаемого файла, не больше ${bytesToMegabytes(
                size_limit
            )}; Ваш файл: ${bytesToMegabytes(file.size)}`
        );
    }
    return isAllowedType && isAllowedSize;
};

export function getBase64(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result as string);
        reader.onerror = (error) => reject(error);
    });
}

export const MIME_TYPES = [
    "image/jpeg",
    "image/png",
    "image/svg",
    "image/svg+xml",
    "application/json",
];
