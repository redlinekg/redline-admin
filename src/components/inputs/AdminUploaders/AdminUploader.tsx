import React, { PureComponent } from "react";
import Form from "antd/es/form";
import Icon from "antd/es/icon";
import message from "antd/es/message";
import Upload from "antd/es/upload";
import { UploadChangeParam } from "antd/es/upload/interface";
import CN from "classnames";
import { connect } from "formik";
import get from "lodash/get";
import { styled } from "linaria/react";

import {
    AdminInputChangeValue,
    AdminInputProps,
    FormikAdminInputProps,
    MimeTypes,
    UploadType,
    UrlExecutor,
} from "../interfaces";
import { createFileValidator, MIME_TYPES } from "./util";

export interface AdminUploaderProps
    extends AdminInputProps,
        AdminInputChangeValue {
    upload_url: string;
    url_executor?: UrlExecutor;
    form_data_field?: string;
    allowed_types?: MimeTypes;
    size_limit?: number;
    type?: UploadType;
}

export interface AdminUploaderDefaultProps {
    url_executor: UrlExecutor;
    form_data_field: string;
    allowed_types: MimeTypes;
    size_limit: number;
    type: UploadType;
}

export interface AdminUploaderState {
    loading: boolean;
    imageUrl: string | null;
}

class AdminUploader extends PureComponent<
    AdminUploaderProps & FormikAdminInputProps,
    AdminUploaderState
> {
    static defaultProps: AdminUploaderDefaultProps = {
        allowed_types: MIME_TYPES,
        url_executor: (response) => response.path,
        form_data_field: "file",
        size_limit: 1024 * 1024 * 2, // by default: 2MB
        type: "image",
    };

    state: AdminUploaderState = {
        loading: false,
        imageUrl: null,
    };

    handleChange = (info: UploadChangeParam) => {
        const {
            url_executor,
            onChange,
            formik: { setFieldValue },
            name,
        } = this.props as AdminUploaderProps &
            AdminUploaderDefaultProps &
            FormikAdminInputProps;
        if (info.file.status === "uploading") {
            this.setState({ loading: true });
        } else if (info.file.status === "done") {
            setFieldValue(name, info.file.response);
            this.setState({
                imageUrl: url_executor(info.file.response),
                loading: false,
            });
            if (onChange) {
                onChange(info.file.response);
            }
        } else if (info.file.status === "error") {
            message.error("Произошла ошибка при загрузке файла");
        }
    };

    render() {
        let { imageUrl } = this.state;
        const {
            formik: { values, errors },
            name,
            url_executor,
            allowed_types,
            size_limit,
            upload_url,
            form_data_field,
            label,
            help,
            type,
        } = this.props as AdminUploaderProps &
            AdminUploaderDefaultProps &
            FormikAdminInputProps;
        const value = get(values, name);
        if (!imageUrl && value) {
            imageUrl = url_executor(value);
        }
        const error = get(errors, name);
        return (
            <Form.Item
                label={
                    <SLabel>
                        {label}
                        {help ? <SHelp>{help}</SHelp> : null}
                    </SLabel>
                }
                colon={help ? false : true}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <Upload
                    name={form_data_field}
                    listType={type === "image" ? "picture-card" : "text"}
                    className={CN({
                        "avatar-uploader": type === "image",
                    })}
                    showUploadList={false}
                    action={upload_url}
                    beforeUpload={createFileValidator(
                        allowed_types,
                        size_limit
                    )}
                    onChange={this.handleChange}
                >
                    {imageUrl ? (
                        <img
                            src={imageUrl}
                            alt="avatar"
                            style={{ width: "100%" }}
                        />
                    ) : (
                        <div>
                            <Icon
                                type={this.state.loading ? "loading" : "plus"}
                            />
                            <div className="ant-upload-text">Добавить</div>
                        </div>
                    )}
                </Upload>
            </Form.Item>
        );
    }
}

const SLabel = styled.div`
    display: inline-block;
    line-height: 1.3;
`;

const SHelp = styled.div`
    color: var(--grey);
    white-space: normal;
    font-size: 0.9em;
`;

export default connect(AdminUploader);
