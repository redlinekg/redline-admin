import Form from "antd/es/form";
import Select from "antd/es/select";
import { connect, Field, FieldProps } from "formik";
import get from "lodash/get";
import React from "react";
import {
    AdminInputChangeEvent,
    AdminInputProps,
    FormikAdminInputProps,
    SelectVariant,
    ValueConverter,
    ValuePreparer,
    VariantValuePreparer,
} from "./interfaces";

export interface AdminSelectProps
    extends AdminInputProps,
        AdminInputChangeEvent {
    variants: SelectVariant[];
    validate?: () => {};
    onChange: (...args: any[]) => {};
    prepareVariantValue?: VariantValuePreparer;
    prepareValue?: ValuePreparer;
    convertValue?: ValueConverter;
}

export interface AdminSelectDefaultProps {
    prepareVariantValue: VariantValuePreparer;
    prepareValue: ValuePreparer;
    convertValue: ValueConverter;
}

/**
prepareVariantValue - returns value that will be passed to Select.Option
prepareValue - prepare incomming value (passed to Select)
convertValue - returns values that will be send to onChange
---
variants must be like: {value: <>, label: <>}
 */
class AdminSelect extends React.PureComponent<
    AdminSelectProps & FormikAdminInputProps
> {
    static defaultProps = {
        prepareVariantValue: (v: any) => v,
        prepareValue: (v: any) => v,
        convertValue: (v: any) => v,
    };
    render() {
        const {
            variants,
            prepareValue,
            prepareVariantValue,
            convertValue,
            label,
            name,
            formik: { errors },
            validate,
            onChange,
            translate_lang,
            ...rest
        } = this.props as AdminSelectProps &
            AdminSelectDefaultProps &
            FormikAdminInputProps;
        translate_lang;
        const error = get(errors, name);
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <Field name={name} validate={validate}>
                    {({
                        field: { value },
                        form: { setFieldValue, setFieldTouched },
                    }: FieldProps<any>) => (
                        <Select
                            onChange={(value: any, option: any) => {
                                value = convertValue(value);
                                setFieldValue(name, value);
                                onChange && onChange(value, option);
                            }}
                            onBlur={() => setFieldTouched(name)}
                            value={prepareValue(value)}
                            notFoundContent="Не найдено"
                            {...rest}
                        >
                            {variants.map((v, idx) => (
                                <Select.Option
                                    key={idx}
                                    value={prepareVariantValue(v.value)}
                                >
                                    {v.label}
                                </Select.Option>
                            ))}
                        </Select>
                    )}
                </Field>
            </Form.Item>
        );
    }
}

export default connect(AdminSelect);
