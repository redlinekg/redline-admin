import { Switch } from "formik-antd";
import Form from "antd/es/form";
import { connect } from "formik";
import React from "react";
import {
    AdminInputProps,
    FormikAdminInputProps,
    AdminInputChangeEvent,
} from "./interfaces";
import get from "lodash/get";

export interface AdminCheckboxProps
    extends AdminInputProps,
        AdminInputChangeEvent {}

class AdminCheckbox extends React.PureComponent<
    AdminCheckboxProps & FormikAdminInputProps
> {
    // static propTypes = {
    //     ...form_item_props,
    //     ...admin_input_props
    // };
    render() {
        const {
            label,
            name,
            formik: { errors },
            translate_lang,
            ...rest
        } = this.props;
        translate_lang;
        const error = get(errors, name);
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <Switch name={name} fast={true} {...rest} />
            </Form.Item>
        );
    }
}

export default connect(AdminCheckbox);
