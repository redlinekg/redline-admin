import { InputNumber } from "formik-antd";
import Form from "antd/es/form";
import { connect } from "formik";
import get from "lodash/get";
import React from "react";
import {
    AdminInputProps,
    FormikAdminInputProps,
    AdminInputChangeEvent,
} from "./interfaces";

export interface AdminNumberInputProps
    extends AdminInputProps,
        AdminInputChangeEvent {}

class AdminNumberInput extends React.PureComponent<
    AdminNumberInputProps & FormikAdminInputProps
> {
    // static propTypes = {
    //     ...form_item_props,
    //     ...admin_input_props
    // };
    render() {
        const {
            label,
            name,
            formik: { errors },
            translate_lang,
            ...rest
        } = this.props;
        translate_lang;
        const error = get(errors, name);
        return (
            <Form.Item
                label={label}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <InputNumber
                    name={name}
                    fast={true}
                    {...rest}
                    defaultValue={1}
                />
            </Form.Item>
        );
    }
}

export default connect(AdminNumberInput);
