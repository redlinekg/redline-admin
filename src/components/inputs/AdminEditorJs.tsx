import React from "react";
import EditorJS from "@editorjs/editorjs";
import { styled } from "linaria/react";
import Form from "antd/es/form";
import axios from "axios";
import { isBrowser } from "browser-or-node";
import { connect } from "formik";
import get from "lodash/get";
import memoize from "lodash/memoize";

import {
    AdminInputChangeValue,
    AdminInputProps,
    FormikAdminInputProps,
} from "./interfaces";

const EditorJs = isBrowser ? require("react-editor-js").default : null;
const Paragraph = isBrowser ? require("@editorjs/paragraph") : null;
const ImageTool = isBrowser ? require("@editorjs/image") : null;
const Header = isBrowser ? require("@editorjs/header") : null;
const Checklist = isBrowser ? require("@editorjs/checklist") : null;
const List = isBrowser ? require("@editorjs/list") : null;
const Quote = isBrowser ? require("@editorjs/quote") : null;
const Raw = isBrowser ? require("@editorjs/raw") : null;
const Embed = isBrowser ? require("@editorjs/embed") : null;
const Delimiter = isBrowser ? require("@editorjs/delimiter") : null;
const Warning = isBrowser ? require("@editorjs/warning") : null;
const LinkTool = isBrowser ? require("@editorjs/link") : null;
const Marker = isBrowser ? require("@editorjs/marker") : null;
const Attaches = isBrowser ? require("@editorjs/attaches") : null;

import { down } from "../../config/vars";

const createImageUploader = (
    image_upload_url: string,
    image_form_data_field: string,
    image_url_executor: (response: any) => string
) => ({
    uploadByFile(file: File) {
        const fd = new FormData();
        fd.append(image_form_data_field, file);
        return axios
            .post(image_upload_url, fd)
            .then((response) => {
                return {
                    success: 1,
                    file: {
                        url: image_url_executor(response.data),
                    },
                };
            })
            .catch((error) => {
                console.error("Error on upload image by file", error);
                return {
                    success: 0,
                };
            });
    },
    uploadByUrl(url: string) {
        return axios
            .post(image_upload_url, url)
            .then((response) => {
                return {
                    success: 1,
                    file: {
                        url: image_url_executor(response.data),
                    },
                };
            })
            .catch((error) => {
                console.error("Error on upload image by url", error);
                return {
                    success: 0,
                };
            });
    },
});

export interface AdminEditorJsProps
    extends AdminInputProps,
        AdminInputChangeValue {
    tools: { [key: string]: any };
    exclude?: string[];
    image_upload_url: string;
    image_url_executor: string;
    image_form_data_field?: string;
    file_upload_url: string;
    file_form_data_field?: string;
    file_types: string[];
    link_endpoint: string;
}

export interface AdminEditorJsDefaultProps {
    image_form_data_field: string;
    file_form_data_field: string;
    exclude: string[];
}

/**
 * Props
 * image_upload_url - url to server that must to save file or get url to image and save image by url
 * image_url_executor - fuction that to receives response body and must to return link to the uploaded image
 * image_form_data_field - name of field that contain file in FormData (Default: file)
 */
class AdminEditorJs extends React.PureComponent<
    AdminEditorJsProps & FormikAdminInputProps
> {
    static defaultProps: AdminEditorJsDefaultProps = {
        image_form_data_field: "file",
        file_form_data_field: "file",
        exclude: ["table", "raw", "code", "inlineCode"],
    };

    editor: EditorJS | null = null;
    rebuilded = false;

    changeHandler = async () => {
        const {
            name,
            formik: { setFieldValue },
        } = this.props;
        if (this.editor) {
            const { blocks } = await this.editor.save();
            setFieldValue(name, JSON.stringify(blocks));
        }
    };

    getBlocks = () => {
        const {
            formik: { values },
            name,
        } = this.props;
        let blocks = [];
        try {
            const value = get(values, name);
            if (value) {
                blocks = JSON.parse(value);
            }
        } catch (error) {
            console.error(
                `Cannot to parse in 'AdminEditorJs'; Value: ${get(
                    values,
                    name
                )}`
            );
        }
        return blocks;
    };

    getTools = memoize(
        ({
            image_upload_url,
            image_form_data_field,
            image_url_executor,
            file_upload_url,
            file_form_data_field,
            file_types,
            link_endpoint,
        }) => {
            const tools: { [key: string]: any } = {
                paragraph: {
                    class: Paragraph,
                    inlineToolbar: ["bold", "italic", "link"],
                },
                header: {
                    class: Header,
                    config: {
                        placeholder: "Заголовок",
                    },
                },
                list: {
                    class: List,
                    inlineToolbar: true,
                },
                quote: {
                    class: Quote,
                    inlineToolbar: true,
                    config: {
                        quotePlaceholder: "Текст цитаты",
                        captionPlaceholder: "Подпись цитаты",
                    },
                },
                raw: {
                    class: Raw,
                    hideToolbar: true,
                },
                checklist: {
                    class: Checklist,
                    inlineToolbar: true,
                },
                embed: Embed,
                delimiter: Delimiter,
                warning: Warning,
                marker: Marker,
            };
            if (image_upload_url) {
                tools.image = {
                    class: ImageTool,
                    config: {
                        uploader: createImageUploader(
                            image_upload_url,
                            image_form_data_field,
                            image_url_executor
                        ),
                        placeholder: "Введите подпись",
                    },
                };
            }
            if (link_endpoint) {
                tools.link_tool = {
                    class: LinkTool,
                    config: {
                        endpoint: link_endpoint,
                    },
                };
            }
            if (file_upload_url) {
                tools.attaches = {
                    class: Attaches,
                    config: {
                        endpoint: file_upload_url,
                        field: file_form_data_field,
                        types: file_types ? file_types.join(", ") : undefined,
                        buttonText: "Загрузить",
                        errorMessage: "Загрузка провалилась",
                    },
                };
            }
            return tools;
        }
    );

    componentDidUpdate(prevProps: AdminEditorJsProps & FormikAdminInputProps) {
        this.mustToRebuild(prevProps);
    }

    /**
     * Rebuild editorjs on first change of value
     */
    mustToRebuild(prevProps: AdminEditorJsProps & FormikAdminInputProps) {
        const {
            formik: { values },
            name,
        } = this.props;
        const {
            formik: { values: prevValues },
        } = prevProps;
        const value = get(values, name);
        const prevValue = get(prevValues, name);
        if (!this.rebuilded && value !== prevValue && this.editor) {
            const blocks = this.getBlocks();
            this.editor.isReady.then(() => {
                if (this.editor) {
                    this.editor.render({
                        blocks,
                    });
                }
            });
            this.rebuilded = true;
        }
    }

    render() {
        const {
            label,
            name,
            formik: { errors },
            //
            exclude,
            // image uploader
            image_upload_url,
            image_url_executor,
            image_form_data_field,
            // file uploader
            file_upload_url,
            file_form_data_field,
            file_types,
            // link
            link_endpoint,
            tools,
            translate_lang,
            help,
            ...rest
        } = this.props;
        translate_lang;
        const blocks = this.getBlocks();
        const error = get(errors, name);
        const formItemEditorJs = {
            labelCol: { span: 0 },
            wrapperCol: { span: 24 },
        };
        return (
            <Form.Item
                {...formItemEditorJs}
                validateStatus={error ? "error" : ""}
                help={error}
            >
                <SEditorJs>
                    <STitle>
                        <div>{label}</div>
                        {help}
                    </STitle>
                    <SEditorJsWrap>
                        {isBrowser && EditorJs ? (
                            <EditorJs
                                instanceRef={(instance: EditorJS) =>
                                    (this.editor = instance)
                                }
                                onChange={this.changeHandler}
                                tools={{
                                    ...this.getTools({
                                        exclude,
                                        image_upload_url,
                                        image_url_executor,
                                        image_form_data_field,
                                        file_upload_url,
                                        file_form_data_field,
                                        file_types,
                                        link_endpoint,
                                    }),
                                    ...tools,
                                }}
                                data={{
                                    blocks,
                                }}
                                {...rest}
                            />
                        ) : null}
                    </SEditorJsWrap>
                </SEditorJs>
            </Form.Item>
        );
    }
}

const SEditorJs = styled.div`
    background-color: var(--grey-light4);
    padding: 1em;
    border-radius: var(--global-border-radius);
    ${down("md")} {
        background-color: transparent;
        padding: 0;
    }
    /* ////////// */
    .cdx-quote,
    .cdx-warning,
    .image-tool {
        line-height: 1.6;
    }
    .cdx-checklist__item-text {
        line-height: 1.6;
        padding: 8px 0;
    }
    .cdx-attaches--with-file {
        line-height: 1.6;
    }
    .ce-header {
        font-weight: 800;
    }
`;

const SEditorJsWrap = styled.div`
    background-color: var(--white);
    padding: 1em;
    margin-top: 1em;
    .codex-editor__redactor {
        padding-bottom: 50px !important;
    }
`;

const STitle = styled.div`
    font-size: 1.2em;
    font-weight: 600;
    line-height: 1.2;
`;

export default connect(AdminEditorJs);
