import { createContext } from "react";

interface AdminFormContextType {
    handleDelete?: (v: any) => void;
}

export const AdminFormContext = createContext<AdminFormContextType>({});
