import React from "react";

const RoutesContext = React.createContext<{ routes: any }>({
    routes: null,
});

export default RoutesContext;
