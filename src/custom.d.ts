declare module "*.svg" {
    const content: string;
    export default content;
}

declare module "browser-or-node" {
    const isBrowser: boolean;
    const isNode: boolean;
}
