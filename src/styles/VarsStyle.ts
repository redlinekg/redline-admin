import { css } from "linaria";

import { down, colors, fonts, global, header } from "../config/vars";

css`
    :global() {
        :root {
            /* colors */
            --white: ${colors.white};
            --black: ${colors.black};
            --blue: ${colors.blue};
            --red: ${colors.red};
            --violet: ${colors.violet};
            --yellow: ${colors.yellow};
            --green: ${colors.green};
            --grey: ${colors.grey};
            --grey-bor: ${colors.grey_bor};
            --grey-light: ${colors.grey_light};
            --grey-light2: ${colors.grey_light2};
            --grey-light3: ${colors.grey_light3};
            --grey-light4: ${colors.grey_light4};
            --grey-light5: ${colors.grey_light5};
            --grey-light6: ${colors.grey_light6};

            /* sidebar */
            --sidebar_bg: #1a1a1a;

            /* fonts */
            --ff-global: ${fonts.ff_global};

            /* global */
            --global-padding-content: 20px;
            --global-sidebar-width: ${global.sidebar_width};
            --global-border-radius: ${global.border_radius};
            ${down("md")} {
                --global-padding-content: 15px;
            }

            /* header */
            --header-height: ${header.height};
            --header-height-top: ${header.height_top};
            --header-height-bottom: ${header.height_bottom};
            ${down("md")} {
                --header-height: auto;
                --header-height-top: 48px;
                --header-height-bottom: auto;
            }
        }
    }
`;
