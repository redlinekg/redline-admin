import { css } from "linaria";

import { down } from "../config/vars";

css`
    :global() {
        body {
            font-family: "Harmonia Sans Pro Cyr";
            .ant-popover-message-title {
                padding-left: 1.5em;
            }
            .ant-form-item-label {
                font-weight: 700;
                font-size: 1.1em;
            }
        }
        button {
            outline: none !important;
        }
        .ant-table-small {
            background-color: var(--white);
        }
        .ant-form-item {
            ${down("md")} {
                margin-bottom: 1em;
            }
        }
        .ant-form-item-label {
            ${down("md")} {
                padding-bottom: 0;
            }
        }
    }
`;
