import { css } from "linaria";
import { fontFace } from "polished";

css`
    :global() {
        ${fontFace({
            fontFamily: "Harmonia Sans Pro Cyr",
            fontFilePath:
                "/static/fonts/HarmoniaSans/subset-HarmoniaSansProCyr-Black",
            fileFormats: ["woff2", "woff"],
            fontWeight: "900"
        })};
        ${fontFace({
            fontFamily: "Harmonia Sans Pro Cyr",
            fontFilePath:
                "/static/fonts/HarmoniaSans/subset-HarmoniaSansProCyr-Light",
            fileFormats: ["woff2", "woff"],
            fontWeight: "300"
        })};

        ${fontFace({
            fontFamily: "Harmonia Sans Pro Cyr",
            fontFilePath:
                "/static/fonts/HarmoniaSans/subset-HarmoniaSansProCyr-Regular",
            fileFormats: ["woff2", "woff"],
            fontWeight: "normal"
        })};

        ${fontFace({
            fontFamily: "Harmonia Sans Pro Cyr",
            fontFilePath:
                "/static/fonts/HarmoniaSans/subset-HarmoniaSansProCyr-Bold",
            fileFormats: ["woff2", "woff"],
            fontWeight: "bold"
        })};
    }
`;
