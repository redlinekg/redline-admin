export type RouteParams = {
    [key: string]: string;
};

export interface RouteItem {
    route: string;
    slug: string;
    title?: string;
    sub: RouteItem[];
    icon: string;
    delimiter: boolean;
    group_title: string | null;
}

export type Language = {
    code: string;
    name: string;
};
