module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    plugins: [
        "react",
        "import",
        "@typescript-eslint/eslint-plugin",
        "prettier",
    ],
    extends: [
        "eslint:recommended",
        // "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:import/typescript",
        "plugin:react/recommended",
        "plugin:prettier/recommended",
        "plugin:@typescript-eslint/eslint-plugin/recommended",
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: "module",
    },
    rules: {
        "linebreak-style": ["error", "unix"],
        quotes: ["error", "double"],
        semi: ["error", "always"],
        "require-atomic-updates": "off",
        // prettier
        "prettier/prettier": "error",
        // react
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": 1,
        "react/display-name": "off",
        "react/prop-types": 2,
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/camelcase": "off",
        "@typescript-eslint/no-var-requires": "off",
    },
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    settings: {
        // "import/resolver": {
        //     node: {
        //         extensions: [".js", ".jsx", ".ts", ".tsx", ".es6", ".svg"]
        //     }
        // }
    },
};
