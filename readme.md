# Ну что... это наша админочка

## Instalation

```json5
// package.json
// ...
dependencies: {
    "redline-admin": "git+https://gitlab.com/redlinekg/redline-admin"
}
// ...
```
## [DEV] Сборка

Собирается все крайне просто.. просто набираем `yarn build` и вуаля!

## Требования

Для работы следуте установать:

```bash
yarn add react react-dom react-helmet next next mobx-react mobx-state-tree @emotion/core @emotion/styled
```

#### !!! Помимо кастомных компонентов из `redline-admin` экспортируются:

-   Компоненты `formik`
    ```js
    import { Formik } from "redline-admin";
    ```
-   AntDesign как `antd`
    ```js
    import { antd } from "redline-admin";
    ```

## Структура

-   `/src/config` - Содержит в основном константы для стилей
-   `/src/contexts` - Контексты которые следует шарить между компонентами
-   `/src/styles` - Содержит LESS стили и emotion hocs
-   `/src/static` - Картинки...
-   `/src/components` - Все имеющиеся компоненты
    -   `/src/components/elements` - Элементы, такие как кнопки например
    -   `/src/components/form` - Компоненты связанные с формами в первую очередь `AdminForm` который является основной частью админки
    -   `/src/components/layout` - Компоненты разметки, там находятся компоненты выполняющие роль основных строительных блоков страницы.. например `AdminSidebar` или `AdminHeader`
    -   `/src/components/inputs` - Компоненты инпутов.. думаю, что и так понятно
    -   `/src/components/settings` - Набор компонентов для редактирования настроек, некий редактор конфигов.. Если нужны новые инпуты в нем, то главное чтобы они выдавали строку
    -   `/src/components/table` - Компоненты для отображения таблиц и данных в них
    -   `/src/components/tabs` - Компоненты для отображения табов

## Список Компонентов

### Элементы

-   `Link`
-   `Meta`
-   `AdminContainer`
-   `AdminBackButton`
-   `AdminDeleteButton`
-   `AdminSaveButton`
-   `AdminCreateButton`
-   `AdminButtonConfirmation`
-   `AdminHeaderButtonsPortal`

### Layout

-   `AdminHeader`
-   `AdminContent`
-   `AdminSidebar`
-   `AdminSidebarMenu`

### Icons

-   `IconsSymbol`

### Inputs

-   `AdminTextInput`
-   `AdminTitleInput`
-   `AdminNumberInput`
-   `AdminUploader`
-   `AdminMultiUploader`
-   `AdminCheckbox`
-   `AdminRadio`
-   `AdminSelect`

    Компонент селекта одиночного или множественного(_доку смотреть на официальном сайте antd_)

    #### Props

    -   `prepareVariantValue` - returns value that will be passed to Select.Option

        Внутри это такая штука:

        ```jsx
        <Select.Option key={idx} value={prepareVariantValue(v.value)}>
            {v.label}
        </Select.Option>
        ```

    -   `prepareValue` - prepare incomming value (passed to Select)
    -   `convertValue` - returns values that will be send to onChange
    -   `variants` - `List<{label: string, value: string}>`

-   `AdminTreeSelect`
-   `AdminTextarea`
-   `AdminDatePicker`
-   `AdminEditorJs`
-   `AdminTranslateTab`

### Form

-   `AdminFormBlock`
-   `AdminForm`
    Специальный компонент формы на странице, получает объекты `store`(_хранилище_) и `item`(_запись_). Умеет вызывать соответсвующие методы сторы и записи, например для создания новой записи или редактирования существующей. Чтобы обработать успешную отправку формы из вне, можно передать 4 коллбэка: `onSubmitSuccess`, `onSubmitError`, `onDeleteSuccess`, `onDeleteError`(_на текущий момент времени_).

    **Важно**: Компонент является оберткой вокруг `Formik`, и все пропсы которые в нем не описаны передаются дальше в формик. Например для валидации надо передавать `validationSchema`:

    #### Props

    -   `children` - Содержимое самой формы.. ее элементы.. инпуты и.т.п
    -   `item` - Запись которая редактируется на текущий момент времени в этой форме
    -   `store` - DetailStore какой-то модели.
    -   `remove_show` - Надо ли показывать кнопку удаления или же нет?
    -   `onSubmitSuccess` - Функция которая будет вызвана после удачной отправки формы.
        Почти наверника после отправки формы, понадобится редирект на страницу редактирования добавленной записи.

        > Почти наверника ты будешь делать, что-то такое.

        ```jsx
        <AdminForm
            item={item}
            store={product_store}
            validationSchema={SCHEMA} // Этот props будет передаваться в <Formik/>
            onSubmitSuccess={() => {
                // router from @vlzh/next-routes
                router.pushRoute("products_list");
            }}
        >
            <AdminTextarea name="title" label="Заголовок продукта" />
        </AdminForm>
        ```

    -   `onSubmitError` - Функция которая будет вызвана при ошибке во время отправки формы
    -   `onDeleteSuccess` - Функция которая будет вызвана после удачного удаления записи.
    -   `onDeleteError` - Функция будет вызвана, если при удалении записи возникла ошибка.

    > Обрати внимание на то, что если не передана функция для обработки ошибки, возникшая в ходе сохранения ошибка будет всплывать.

-   `AdminSaveFormButton`

### Table

-   `AdminTableImage` - Изображение в таблице
-   `AdminTableOperation` - Интерактивный блок с кнопками редактирования/удаления
    #### Props
    -   `item` - Mobx model of item
    -   `remove` - Показывать ли кнопку удаления
    -   `movable` - Показывать ли
    -   `route` - Название роута, который отвечает за редактирование этой сущности
    -   `params` - Параметры которые надо подставить в роут
-   `AdminTableTitle` - строка со ссылкой
-   `AdminTableDate` - вывод даты в таблице
    #### Props
    -   `date` - Date
    -   `time_show` - Показывать ли время или только дату
-   `AdminTable` - Основной компонент таблицы
-   `AdminTable` - Основной компонент таблицы
-   `AdminTableStatus` - Компонент статуса в таблице
    #### Props
    ... получает те же prop`сы что и оригинальный компонент из antd

### Tabs

-   `AdminTabs`

### Settings

-   `AdminSettings`
-   `AdminSettingsPropertyAdd`
-   `AdminSettingsSectionAdd`
-   `AdminSettingsSection`
-   `AdminSettingsProperty`
-   `AdminSettingsTextInput`
-   `AdminSettingsNumberInput`
-   `AdminSettingsTextarea`

## Пример

```js
import React, { Component } from "react";
import * as Yup from "yup";
import {
    AdminContent,
    AdminForm,
    AdminTextInput,
    AdminTextarea,
    AdminRadio,
    AdminUploader,
    AdminEditorJs,
    AdminFormRepeater,
    AdminMultiUploader,
} from "redline_admin";
import { Tabs } from "antd";
const { TabPane } = Tabs;
import routes from "../routes";

const schema = Yup.object().shape({
    title: Yup.string().required("Введите заголовок"),
    slug: Yup.string().required("Введите Slug"),
    type: Yup.string().oneOf([true], "Выберите тип портфолио"),
    image: Yup.object(),
    body: Yup.string(),
    info: Yup.array().of(
        Yup.object().shape({
            type: Yup.string().required(),
            value: Yup.string().required(),
        })
    ),
});

export default class PortfolioDetailPage extends Component {
    render() {
        return (
            <AdminContent
                title={
                    (() => false)()
                        ? "Добавление в портфолио"
                        : "Редактирование портфолио"
                }
                routes={routes}
                back={this._Black}
                delite
            >
                <AdminForm is_ready={true} validationSchema={schema}>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Основное" key="1">
                            <AdminTextInput label="Заголовок" name="title" />
                            <AdminTextInput label="Слаг" name="slug" />
                            <AdminRadio
                                label="Тип"
                                name="type"
                                variants={[
                                    { value: "project", label: "Работа" },
                                    { value: "collection", label: "Коллекция" },
                                ]}
                            />
                            <AdminTextarea
                                label="Описание"
                                name="description"
                            />
                            <AdminEditorJs label="Текст статьи" name="body" />
                            <AdminUploader
                                name="image"
                                label="Превью"
                                upload_url="/uploads/image"
                            />
                        </TabPane>
                        <TabPane tab="Инфо" key="2">
                            <AdminFormRepeater name="info">
                                <AdminTextInput
                                    name="type"
                                    placeholder={"Тип"}
                                />
                                <AdminTextInput
                                    name="value"
                                    placeholder={"Значение"}
                                />
                            </AdminFormRepeater>
                        </TabPane>
                        <TabPane tab="Галерея" key="3">
                            <AdminMultiUploader
                                name="image"
                                label="Общая галерея"
                                upload_url="/uploads/image"
                            />
                            <AdminMultiUploader
                                name="image"
                                label="Планировки"
                                upload_url="/uploads/image"
                            />
                        </TabPane>
                        <TabPane tab="Группы фотографий" key="4">
                            <AdminFormRepeater name="photo_groups">
                                <AdminTextInput
                                    name="title"
                                    placeholder={"Название"}
                                />
                                <AdminTextInput
                                    name="description"
                                    placeholder={"Описание"}
                                />
                                <AdminMultiUploader
                                    name="image"
                                    label="Изображение"
                                    upload_url="/uploads/image"
                                />
                            </AdminFormRepeater>
                        </TabPane>
                    </Tabs>
                </AdminForm>
            </AdminContent>
        );
    }
}
```
